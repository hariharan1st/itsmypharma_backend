package com.itsmypharma.common.util;

import com.itsmypharma.modules.medicine.MedicalMaster;
import com.itsmypharma.modules.order.request.OrderStatus;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class PharmaUtil {

	public static final List<Integer> NEW_ORDER_STATUS = Arrays.asList(OrderStatus.CUSTOMER_PLACES_ORDER.getValue());
	public static final List<Integer> PENDING_ORDER_STATUS = Arrays.asList(OrderStatus.PHARMACY_ACCEPTS_ORDER.getValue(), OrderStatus.CUSTOMER_ACCEPTS_REVISION.getValue());
	public static final List<Integer> COMPLETED_ORDER_STATUS = Arrays.asList(OrderStatus.CUSTOMER_REJECTS_REVISION.getValue(), OrderStatus.PHARMACY_REJECTS_ORDER.getValue(),
			OrderStatus.CUSTOMER_CANCELS_BEFORE_PICKUP.getValue(), OrderStatus.CUSTOMER_CANCELS_BEFORE_PHARMACY_ACCEPTS_OR_REJECTS.getValue(),
			OrderStatus.CUSTOMER_NOT_ACCEPTED_TILL_PICKUP_TIME.getValue(), OrderStatus.PHARMACY_NOT_ACCEPTED_TILL_PICKUP_TIME.getValue(),
			OrderStatus.PHARMACY_DELIVERED.getValue());


	public static String encrypt(String value) {
		StringBuilder sb = null;
		try {
			sb = new StringBuilder();
			MessageDigest md = MessageDigest.getInstance("MD5");

			md.update(value.getBytes());

			byte[] bytes = md.digest();

			for (int i = 0; i < bytes.length; i++) {
				sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
			}

		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return sb.toString();
	}
	
	public static String addImagePath(String imagePath, MedicalMaster medicine) {

		Character mediFirstLetter = medicine.getDrugName() != null ? medicine.getDrugName().charAt(0) : null;
		String path = null;
		switch (mediFirstLetter) {
		case 'A':
		case 'B':
		case 'C':
		case 'D':
		case 'E':
		case 'F':
		case 'G':
		case 'H':
		case 'I':
		case 'J':
		case 'K':
		case 'L':
		case 'M':
		case 'N':
		case 'O':
		case 'P':
		case 'Q':
		case 'R':
		case 'S':
		case 'T':
		case 'U':
		case 'V':
		case 'W':
		case 'X':
		case 'Y':
		case 'Z':
			if ("Y".equals(medicine.getIsImageAvailable()))
				path = imagePath + "drugImage_" + mediFirstLetter + "/drugs/" + medicine.getFileLocation() + "/1.jpg";
			else
				path = imagePath+"Default.svg";
				
			break;
		default:
			if ("Y".equals(medicine.getIsImageAvailable()))
				path = imagePath + "drugImage_NoAlpha/drugs/" + medicine.getFileLocation() + "/1.jpg";
			else
				path = imagePath+"Default.svg";

			break;

		}
		return path;

	}

	public static String padLeft(String stringToPad, int padToLength) {
		String retValue = null;
		if (stringToPad.length() < padToLength) {
			retValue = String.format("%0" + String.valueOf(padToLength - stringToPad.length()) + "d%s", 0, stringToPad);
		} else {
			retValue = stringToPad;
		}
		return retValue;
	}
	
	public static String getCurrentDate() {
		Date date = new Date();  
	    SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");  
	    return formatter.format(date);  
	}

}
