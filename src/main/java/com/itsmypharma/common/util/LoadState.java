package com.itsmypharma.common.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class LoadState {

	public static void main(String s[]) throws SQLException, IOException {

		final String DB_URL = "jdbc:mysql://164.52.200.170:3306/itsmypharma?useSSL=false&useUnicode=yes&characterEncoding=UTF-8";

		// Database credentials
		// final String USER = "admin";
		// final String PASS = "itsmypharma012";
		final String USER = "pharma";
		final String PASS = "HGUT^%@gyfd7";
		Connection conn = null;
		PreparedStatement stmt = null;
		Workbook workbook = null;

		Map<String, String> map = new HashMap<>();
		map.put("Daman and Diu", "1");
		map.put("Jammu and Kashmir", "2");
		map.put("Delhi", "3");
		map.put("Himachal Pradesh", "4");
		map.put("Pondicherry", "5");
		map.put("Dadra and Nagar Haveli", "6");
		map.put("Haryana", "7");
		map.put("West Bengal", "8");
		map.put("Bihar", "9");
		map.put("Karnataka", "10");
		map.put("Sikkim", "11");
		map.put("Goa", "12");
		map.put("Maharashtra", "13");
		map.put("Uttar Pradesh", "14");
		map.put("Megalaya", "15");
		map.put("Uttarakhand", "16");
		map.put("Kerala", "17");
		map.put("Manipur", "18");
		map.put("Gujarat", "19");
		map.put("Madhya Pradesh", "20");
		map.put("Odisha", "21");
		map.put("Chandigarh", "22");
		map.put("Andaman and Nico.In.", "23");
		map.put("Mizoram", "24");
		map.put("Andhra Pradesh", "25");
		map.put("Arunachal Pradesh", "26");
		map.put("Chattisgarh", "27");
		map.put("Assam", "28");
		map.put("Punjab", "29");
		map.put("Telangana", "30");
		map.put("Ladakh", "31");
		map.put("Lakshadweep", "32");
		map.put("Rajasthan", "33");
		map.put("Tamil Nadu", "34");
		map.put("Jharkhand", "35");
		map.put("Nagaland", "36");
		map.put("Tripura", "37");

		try {
			Class.forName("com.mysql.jdbc.Driver");

			// STEP 3: Open a connection
			System.out.print("\nConnecting to database...");
			conn = DriverManager.getConnection(DB_URL, USER, PASS);
			conn.setAutoCommit(false);

			System.out.println("connected");
			File folder = new File("/Users/suvins/Downloads/Pincode.xlsx");

			AtomicLong rownum = new AtomicLong(19250);

			FileInputStream excelFile = new FileInputStream(folder);

			workbook = new XSSFWorkbook(excelFile);
			System.out.println("loaded");
			Sheet datatypeSheet = workbook.getSheetAt(0);
			System.out.println("Got sheet 0");
			Iterator<Row> iterator = datatypeSheet.iterator();
			int i = 0;
			boolean flag = false;
			stmt = conn.prepareStatement(
					"insert into pharma_city_mast( pcm_pin_code,pcm_city_name,pcm_state_id,pcm_city_id) values(?,?,?,?)");
			while (iterator.hasNext()) {

				Row currentRow = iterator.next();
				if (i == 0||i<19250) {
					i++;
					System.out.println("continue");
					continue;
				}

				if (flag)
					break;
				int j = 1;
				for (int z = 0; z < 4; z++) {

					Cell currentCell = currentRow.getCell(z);
					
					switch (j) {
					case 1:
						flag = currentCell == null ? true : false;
						if (flag)
							break;
						stmt.setString(1, String.valueOf((int)currentCell.getNumericCellValue()));
						System.out.println("pincode" + currentCell == null ? null : String.valueOf((int)currentCell.getNumericCellValue()));
						j++;
						break;
					case 2:

						stmt.setString(2, currentCell.getStringCellValue());
						System.out.println("city code" + currentCell == null ? null : currentCell.getStringCellValue());
						j++;
						break;
					case 3:

						if (map.get(currentCell.getStringCellValue()) == null)
							throw new Exception("State code not found");
						stmt.setString(3, map.get(currentCell.getStringCellValue()));
						System.out.println("statecode" + currentCell == null ? null : currentCell.getStringCellValue());
						j++;
						break;
					case 4:

						stmt.setInt(4, Integer.parseInt(String.valueOf(rownum.getAndIncrement())));
						System.out.println("id" + rownum.get());
						j++;
						break;
					default:
						j++;
						break;
					}

				}
				stmt.addBatch();
				System.out.println(Integer.parseInt(rownum.toString()) - 1 + "records added");
				if (Integer.parseInt(rownum.toString()) % 10 == 0) {
					System.out.println("Started.........................");
					stmt.executeLargeBatch();
					stmt.close();
					conn.commit();
					conn.close();

					conn = DriverManager.getConnection(DB_URL, USER, PASS);
					conn.setAutoCommit(false);
					stmt = conn.prepareStatement(
							"insert into pharma_city_mast( pcm_pin_code,pcm_city_name,pcm_state_id,pcm_city_id) values(?,?,?,?)");
				}
			}

			System.out.println(Integer.parseInt(rownum.toString()) - 1 + "records added");

			System.out.println("Started final records.........................");
			stmt.executeLargeBatch();
			stmt.close();
			conn.commit();
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
			conn.rollback();
		} finally {
			workbook.close();
		}
	}

}
