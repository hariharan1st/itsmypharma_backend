package com.itsmypharma.common.util;

import lombok.Data;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Data
public class ResponseData {
    private boolean success;
    private Object data;
    private String message;

    public ResponseData(boolean success, Object data) {
        this.success = success;
        if(!this.success) {
            this.message = (String) data;
        }
        this.data = data;
    }

    public static ResponseData setSuccess(Object data) {
        return new ResponseData(true, data);
    }

    public static ResponseData setSuccess(List list, Integer totalPages) {
        Map responseMap = new HashMap();
        responseMap.put("list", list);
        responseMap.put("totalPages", totalPages);
        return new ResponseData(true, responseMap);
    }

    public static ResponseData setFailure(String message) {
        return new ResponseData(false, message);
    }

}
