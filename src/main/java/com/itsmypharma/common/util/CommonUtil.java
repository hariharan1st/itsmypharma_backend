package com.itsmypharma.common.util;

import com.itsmypharma.constants.Constants;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;
import org.springframework.beans.NotReadablePropertyException;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.multipart.MultipartFile;

import javax.net.ssl.HttpsURLConnection;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;

@Slf4j
public class CommonUtil {
    public static boolean isCurrentUserBelongsToRole(String roleName) {
        Collection<? extends GrantedAuthority> authorities = SecurityContextHolder.getContext().getAuthentication().getAuthorities();

        Iterator<? extends GrantedAuthority> iterator = authorities.iterator();
        while (iterator.hasNext()) {
            GrantedAuthority grantedAuthority = iterator.next();
            if (grantedAuthority.getAuthority().equals(roleName))
                return true;
        }
        return false;
    }

    public static String getOtp(int digits) {
        Random random = new Random();
        String format = "%0" + digits + "d";
        String otp = String.format(format, random.nextInt(10000));
        return otp;
    }

    public static boolean isAdminUser() {
        return isCurrentUserBelongsToRole("ROLE_ADMIN");
    }

    public static Integer getCurrentUserAuthId() {
        // principal is the user_auth_id:::store_id
        String[] authKeys = SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString().split(":::");
        if(authKeys.length > 0 && !"null".equals(authKeys[0])) {
            return Integer.parseInt(authKeys[0]);
        }
        return null;
    }

    public static Integer getCurrentStoreId() {
        // principal is the user_auth_id:::store_id
        String[] authKeys = SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString().split(":::");
        if(authKeys.length > 1 && !"null".equals(authKeys[1])) {
            return Integer.parseInt(authKeys[1]);
        }
        return null;
    }

    public static Object copyNonNullProperties(Object source, Object target) {
//        if (source == null || target == null || target.getClass() != source.getClass()) return null;

        final BeanWrapper src = new BeanWrapperImpl(source);
        final BeanWrapper trg = new BeanWrapperImpl(target);

        for (final Field property : target.getClass().getDeclaredFields()) {
            try {
                copyProperty(src, trg, property.getName());
            } catch (NotReadablePropertyException e) {

            }
        }
        return target;
    }

    public static void copyProperty(BeanWrapper src, BeanWrapper trg, String propertyName) {
           Object providedObject = src.getPropertyValue(propertyName);
            // avoid updating collection objects
            if (providedObject != null && !(providedObject instanceof Collection<?>)) {
                trg.setPropertyValue(
                        propertyName,
                        providedObject);
            }
    }

    public static String getRequestParamValue(Map<String, String[]> requestMap, String key) {
        String[] values = requestMap.get(key);
        StringBuilder value = new StringBuilder();
        if(values != null && values.length > 0) {
           for(String tempValue : values) {
               value.append(tempValue + ",");
           }
           value.deleteCharAt(value.length()-1);
           return value.toString();
        }
        return null;
    }

    public static void setProperty(List targetObjs, String propertyName, Object propertyValue) {
        if(targetObjs != null) {
           for(Object target: targetObjs) {
               final BeanWrapper trg = new BeanWrapperImpl(target);
               trg.setPropertyValue(propertyName, propertyValue);
           }
        }
    }

    public static Object getRequestParamValue(Map<String, String[]> parameterMap, Object requestBody, String propertyName) {
        Object value = getRequestParamValue(parameterMap, propertyName);
        if(value == null && requestBody != null) {
            final BeanWrapper sentObject = new BeanWrapperImpl(requestBody);
            value = sentObject.getPropertyValue(propertyName);
        }
        return value;
    }

    public  static File multipartToFile(MultipartFile multipart) throws IllegalStateException, IOException {
        File convFile = new File(System.getProperty("java.io.tmpdir")+"/"+ multipart.getOriginalFilename());
        multipart.transferTo(convFile);
        return convFile;
    }

    public static Pageable getDefaultPage(Integer offset) {
        if(offset == null) {
            offset = 0;
        }
        return PageRequest.of(offset, Constants.PAGINATION_SIZE);
    }

    public static String getPostDataString(HashMap<String, String> params) throws UnsupportedEncodingException {
        StringBuilder result = new StringBuilder();
        boolean first = true;
        for(Map.Entry<String, String> entry : params.entrySet()){
            if (first)
                first = false;
            else
                result.append("&");
            result.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(entry.getValue(), "UTF-8"));
        }
        return result.toString();
    }

    public static String sendHttpGetRequest(String urlString, HashMap requestParams) {
        String response = "";
        try {
            String requestURL = urlString + "?" + CommonUtil.getPostDataString(requestParams);
            log.info("sending http request ::: " + urlString);
            URL url = new URL(requestURL);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            int responseCode = conn.getResponseCode();
            if (responseCode == HttpsURLConnection.HTTP_OK) {
                BufferedReader br=new BufferedReader(new InputStreamReader(conn.getInputStream()));
                String line = br.readLine();
                while(line != null) {
                    response+=line;
                    line=br.readLine();
                }
                log.info(response);
            }
            else {
                response = "Error sending request " + responseCode;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }
}
