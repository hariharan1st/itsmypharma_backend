package com.itsmypharma.common.util;

import com.itsmypharma.modules.order.Order;

public class CopyUtil {

    public static Order deepCopy(Order existingOrder) {
        Order newOrder = new Order();
        newOrder.setBillNumber(existingOrder.getBillNumber());
        newOrder.setStatus(existingOrder.getStatus());
        newOrder.setOrderNumber(existingOrder.getOrderNumber());
        newOrder.setEstimatedPrice(existingOrder.getEstimatedPrice());
        newOrder.setComments(existingOrder.getComments());
        newOrder.setDeliveryTime(existingOrder.getDeliveryTime());
        newOrder.setCustomer(existingOrder.getCustomer());
        newOrder.setStore(existingOrder.getStore());
        return newOrder;
    }
}
