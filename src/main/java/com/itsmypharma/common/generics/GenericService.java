package com.itsmypharma.common.generics;

import com.itsmypharma.common.audit.EntityState;
import com.itsmypharma.common.exceptions.EntityNotFoundException;
import com.itsmypharma.common.exceptions.ErrorHolder;
import com.itsmypharma.common.scope.RequestHeaderEnum;
import com.itsmypharma.common.scope.RequestParamEnum;
import com.itsmypharma.common.scope.RequestParams;
import com.itsmypharma.common.util.CommonUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.data.repository.CrudRepository;
import org.springframework.http.HttpMethod;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindException;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.validation.Validator;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

@Transactional
public abstract class GenericService<T extends GenericEntity> {

    protected final Logger log = LoggerFactory.getLogger(GenericService.class);
    @Autowired
    protected HttpServletRequest httpRequest;
    protected CrudRepository genericRepository;
    @Autowired
    private Environment environment;

    @Autowired
    Validator validator;
    private String entityName;

    public GenericService(CrudRepository genericRepository, String entityName) {
        this.genericRepository = genericRepository;
        this.entityName = entityName;
    }

    public String getEntityName() {
        return entityName;
    }

    public void setEntityName(String entityName) {
        this.entityName = entityName;
    }

    public List<T> findAll() {
        return findAll(null);
    }

    public T findByIdElseThrowError(Object id) {
        if(id == null) {
            ErrorHolder.throwError(getEntityName(), "null does not exists");
        }
        try {
            return (T) genericRepository.findById(id).orElseThrow(() -> new EntityNotFoundException(entityName, id));
        } catch (Throwable throwable) {
            throw new EntityNotFoundException(entityName, id);
        }
    }

    public T findById(Object id) {
        try {
            return (T) genericRepository.findById(id).orElseThrow(() -> new EntityNotFoundException(entityName, id));
        } catch (Throwable e) {
            log.info(e.getMessage());
            return null;
        }
    }

    public final T save(T entity) {
        // TODO: entity id has been updated, please check for all usecases
        // previously it was null, now id is being given
        // what if the user changes by passing data for a different user??
        return save(entity.getId(), entity);
    }

    public T save(Object id, T newEntity) {
        if (id != null) {
            // update case
            newEntity.setId(id);
        } else {
            // new entity - save
            newEntity.setCreationDate(System.currentTimeMillis());
        }
        newEntity.setUpdatedDate(System.currentTimeMillis());
        validate(newEntity);
        return saveWithoutValidation(newEntity);
    }

    protected T saveWithoutValidation(T entity) {
        EntityState entityState = EntityState.CREATE;
        if(entity.getId() != null) {
            entityState = EntityState.UPDATE;
        }
        T savedEntity = (T) genericRepository.save(entity);
        postSave(savedEntity, entityState);
        return savedEntity;
    }

    protected void postSave(T savedEntity, EntityState entityState) {

    }

    public T saveAndRefresh(T newEntity) {
        validate(newEntity);
        genericRepository.save(newEntity);
        newEntity.populateTransientValues(this);
        return newEntity;
    }

    public final void delete(Object id) {
        delete(findById(id));
    }

    public void delete(T entity) {
        if(entity == null) {
            throw new EntityNotFoundException(getEntityName(), null);
        }
        genericRepository.delete(entity);
        postSave(entity, EntityState.DELETE);
    }

    public <T extends GenericEntity> void validate(List<T> newEntities) {
        if(newEntities == null) {
            return;
        }
        for (T newEntity: newEntities) {
            validate(newEntity);
        }
    }

    public <T extends GenericEntity> void validate(T newEntity) {
        ErrorHolder errorHolder = new ErrorHolder();
        preFieldValidation(newEntity, errorHolder);
        BindException errors = new BindException(newEntity, newEntity.toString());
        validator.validate(newEntity, errors);
        if (errors.hasErrors()) {
            for (ObjectError objectError : errors.getAllErrors()) {
                FieldError fieldError = (FieldError) objectError;
                errorHolder.addError(getEntityName() + " - " + fieldError.getField(), fieldError.getDefaultMessage());
            }
        }
        if (!errorHolder.hasErrors())
            postFieldValidation(newEntity, errorHolder);
        if (errorHolder.hasErrors())
            throw errorHolder;
    }

    /***
     * method to set properties pre-validation
     * @param <T>
     * @param newEntity
     */

    public <T extends GenericEntity> void preFieldValidation(T newEntity, ErrorHolder errorHolder) throws ErrorHolder {

    }

    /***
     * method to set properties post-validation
     * @param <T>
     * @param newEntity
     */

    public <T extends GenericEntity> void postFieldValidation(T newEntity, ErrorHolder errorHolder) throws ErrorHolder {

    }

    protected boolean isNewObject(Object entity) {
        // TODO: have a mechanism to know whether its new entity or not
        return HttpMethod.POST.name().equals(httpRequest.getMethod()) && ((T)entity).getId() == null;
    }

    public List findAll(Integer offset) {
        List<T> returnList = new ArrayList<>();
        genericRepository.findAll().forEach(o -> returnList.add((T) o));
        return returnList;
    }

    @Autowired
    RequestParams requestParams;
    public Object getRequestParam(RequestParamEnum key) {
        return requestParams.getRequestParam(key);
    }

    public void setRequestParam(RequestParamEnum key, Object value) {
        requestParams.putRequestParam(key, value);
    }

    public void populateRequestParams(Map<String, String[]> parameterMap, T requestBody, HttpMethod httpMethod) {
        // child entities will work on this
        if(HttpMethod.DELETE.equals(httpMethod)) {
            Object forceDelete = CommonUtil.getRequestParamValue(parameterMap, requestBody, "force_delete");
            if("true".equals(forceDelete)) {
                forceDelete = Boolean.TRUE;
            } else {
                forceDelete = Boolean.FALSE;
            }
            setRequestParam(RequestParamEnum.FORCE_DELETE, forceDelete);
        }
    }

    public void save(Collection<T> newEntites) {
        if(newEntites == null) {
            return;
        }
        for(T newEntity: newEntites) {
            save(newEntity);
        }
    }

    public String getHeader(String key) {
        return httpRequest.getHeader(key);
    }

    public Integer getStoreIdFromRequest() {
        String storeId = getHeader(RequestHeaderEnum.STORE_ID.getValue());
        if(storeId == null) {
            ErrorHolder.throwError("store_id not found in header");
        }
        return Integer.parseInt(storeId);
    }

    public String getStoreIdFromRequestPassivated() {
        String storeId = getHeader(RequestHeaderEnum.STORE_ID.getValue());
        if(storeId == null) {
            // return null rather than the error
            return null;
        }
        return storeId;
    }

    public Integer getUserAuthIdFromRequest() {
        String storeId = getHeader(RequestHeaderEnum.USER_AUTH_ID.getValue());
        if(storeId == null) {
            ErrorHolder.throwError("customer_id not found in header");
        }
        return Integer.parseInt(storeId);
    }

    public String getApplicationProperty(String key) {
        return environment.getProperty(key);
    }
}
