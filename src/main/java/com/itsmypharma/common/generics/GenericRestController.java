package com.itsmypharma.common.generics;

import com.itsmypharma.common.exceptions.EntityNotFoundException;
import com.itsmypharma.common.util.CommonUtil;
import com.itsmypharma.common.util.ResponseData;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@Slf4j
public abstract class GenericRestController<T extends GenericEntity, S extends GenericService> {
    protected List<HttpMethod> supportedMethods;

    @Autowired
    HttpServletRequest request;

    public GenericRestController() {
        this(null);
    }

    public GenericRestController(List<HttpMethod> supportedMethods) {
        if(supportedMethods == null) {
            // block all requests
            supportedMethods = new ArrayList<>();
        }
        this.supportedMethods = supportedMethods;
    }

    protected void isMethodSupported(HttpMethod httpMethod, T entity) throws Throwable {
        if (!this.supportedMethods.contains(httpMethod)) {
            List<String> supportedMethodsInString = new ArrayList<>();
            this.supportedMethods.forEach(httpMethod1 -> supportedMethodsInString.add(httpMethod1.toString()));
            throw new HttpRequestMethodNotSupportedException(httpMethod.toString(), supportedMethodsInString);
        }
        else {
            getService().populateRequestParams(request.getParameterMap(), entity, httpMethod);
        }
    }

    @RequestMapping(value = "/set", produces = MediaType.APPLICATION_JSON_UTF8_VALUE, method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    ResponseData postEntity(@RequestBody T entity) throws Throwable {
        isMethodSupported(HttpMethod.POST, entity);
        return ResponseData.setSuccess(getService().save(entity));
    }

    @RequestMapping(produces = MediaType.APPLICATION_JSON_UTF8_VALUE, method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.OK)
    ResponseData putEntity(@RequestBody T newEntity, @RequestParam String id) throws Throwable {
        isMethodSupported(HttpMethod.PUT, newEntity);
        T existingEntity = (T) getService().findById(id);
        if(existingEntity == null) {
            throw new EntityNotFoundException(getService().getEntityName(), id);
        }
        return ResponseData.setSuccess(getService().save(id, newEntity));
    }

    @RequestMapping(produces = MediaType.APPLICATION_JSON_UTF8_VALUE, method = RequestMethod.PATCH)
    @ResponseStatus(HttpStatus.OK)
    ResponseData patchEntity(@RequestBody T newEntity, @RequestParam String id) throws Throwable {
        isMethodSupported(HttpMethod.PATCH, newEntity);
        T existingEntity = (T) getService().findById(id);
        if(existingEntity == null) {
            throw new EntityNotFoundException(getService().getEntityName(), id);
        }
        CommonUtil.copyNonNullProperties(newEntity, existingEntity);
        return ResponseData.setSuccess(getService().save(id, existingEntity));
    }

    @DeleteMapping()
    @ResponseStatus(HttpStatus.NO_CONTENT)
    void deleteEntity(@RequestParam String id) throws Throwable {
        isMethodSupported(HttpMethod.DELETE, null);
        T entity = (T) getService().findById(id);
        if(entity == null) {
            throw new EntityNotFoundException(getService().getEntityName(), id);
        }
        getService().delete(entity);
    }

    @Transactional(readOnly = true)
    @RequestMapping(value = "/get", produces = MediaType.APPLICATION_JSON_UTF8_VALUE, method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    ResponseData getEntities(@RequestParam(required = false) String id, @RequestParam(required = false) Integer offset) throws Throwable {
        isMethodSupported(HttpMethod.GET, null);
        if (id != null) {
            T entity = (T) getService().findById(id);
            return ResponseData.setSuccess(entity);
        } else {
            if(offset == null) {
                offset = 0;
            }
            List entityList = getService().findAll(offset);
            if(entityList.size() == 1) {
                return ResponseData.setSuccess(entityList.get(0));
            } else {
                return ResponseData.setSuccess(entityList);
            }
        }
    }
    protected abstract S getService();
}


