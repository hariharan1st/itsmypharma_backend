package com.itsmypharma.common.generics;

import lombok.Data;

import java.io.Serializable;


@Data
public abstract class GenericEntity<T> implements Serializable {
    public GenericEntity() {
    }

    public void populateTransientValues(GenericService service) {
    }

    public abstract T getId();
    public abstract void setId(T objectId);

    public abstract Long getCreationDate();
    public abstract void setCreationDate(Long date);

    public abstract Long getUpdatedDate();
    public abstract void setUpdatedDate(Long date);
}
