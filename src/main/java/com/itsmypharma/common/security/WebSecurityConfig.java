package com.itsmypharma.common.security;

import com.itsmypharma.common.security.userauth.AuthTokenFilter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@EnableWebSecurity
@Configuration
@Slf4j
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
    @Autowired
    private AuthTokenFilter authTokenFilter;

    public WebSecurityConfig() {
        log.info("WebSecurityConfig Init");
    }

    @Override
    protected void configure(HttpSecurity httpSecurity) throws Exception {
        httpSecurity.cors().and().csrf().disable()
                .authorizeRequests()
                .antMatchers("/**").permitAll()
              /*  .antMatchers("/user/user_exists").permitAll()
                .antMatchers("/user/register_user").permitAll()
                .antMatchers("/user/verify_user").permitAll()
                .antMatchers("/user/verify_otp").permitAll()
                .antMatchers("/user/login").permitAll()
                .antMatchers("/image/*").permitAll()
                .antMatchers("/user/reset_password").permitAll()
                .anyRequest().authenticated()*/
                .and().sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS);
        //.and().httpBasic();
        httpSecurity.addFilterBefore(authTokenFilter, UsernamePasswordAuthenticationFilter.class);
    }
}

