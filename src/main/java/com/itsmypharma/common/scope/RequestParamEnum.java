package com.itsmypharma.common.scope;

public enum RequestParamEnum {
    CURRENT_CUSTOMER("CURRENT_CUSTOMER"),
    CURRENT_USER_AUTH("CURRENT_USER_AUTH"),
    CURRENT_STORE("CURRENT_STORE"),
    SKIP_POST_VALIDATION("SKIP_POST_VALIDATION"),
    REQUEST_STORE_ID("REQUEST_STORE_ID"),
    FORCE_DELETE("FORCE_DELETE"),
    EXISTING_ORDER("EXISTING_ORDER"),
    ORDER_TOTAL_PAGES("ORDER_TOTAL_PAGES");
    private String value;

    RequestParamEnum(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
