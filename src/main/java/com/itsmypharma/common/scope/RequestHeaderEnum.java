package com.itsmypharma.common.scope;

public enum RequestHeaderEnum {
    USER_AUTH_ID("user_auth_id"),
    STORE_ID("store_id");
    private String value;

    RequestHeaderEnum(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
