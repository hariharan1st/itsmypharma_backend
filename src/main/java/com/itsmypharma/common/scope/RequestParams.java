package com.itsmypharma.common.scope;

import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;

import java.util.HashMap;

@Component
@Scope(value = "request", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class RequestParams {
    private HashMap<String, Object> requestParams;

    public RequestParams() {
        requestParams = new HashMap<>();
    }

    private HashMap<String, Object> getRequestParams() {
        return requestParams;
    }

    public Object getRequestParam(RequestParamEnum requestParamEnum) {
        return getRequestParams().get(requestParamEnum.getValue());
    }

    public Boolean getBoolean(RequestParamEnum requestParamEnum) {
        Object value = getRequestParam(requestParamEnum);
        if (value == null) {
            return false;
        }
        return (Boolean) value;
    }

    public Integer getInteger(RequestParamEnum key) {
        return (Integer) getRequestParam(key);
    }

    public String getString(RequestParamEnum key) {
        return (String) getRequestParam(key);
    }

    public void putRequestParam(RequestParamEnum key, Object value) {
        getRequestParams().put(key.getValue(), value);
    }
}
