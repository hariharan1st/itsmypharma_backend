package com.itsmypharma.common.audit;

public enum EntityState {
    CREATE(1),
    UPDATE(2),
    DELETE(3);

    private Integer value;

    EntityState(Integer value) {
        this.value = value;
    }

    public Integer getValue() {
        return value;
    }

}
