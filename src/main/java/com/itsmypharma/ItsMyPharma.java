package com.itsmypharma;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ItsMyPharma {
    public static void main(String[] args) {
        SpringApplication.run(ItsMyPharma.class, args);
    }
}
