package com.itsmypharma.constants;

import org.springframework.http.HttpMethod;

import java.util.Arrays;
import java.util.List;

public class Constants {
    public static final Integer PAGINATION_SIZE = 10;
    public static final List ALL_METHODS = Arrays.asList(HttpMethod.GET, HttpMethod.POST, HttpMethod.PATCH, HttpMethod.PUT, HttpMethod.DELETE);
}
