package com.itsmypharma.constants;

public class ResourceBundleConstants {

	private ResourceBundleConstants() {
		throw new IllegalStateException("Utility class");
	}

	
	
	public static final String CONSUMER_ORDER_WITH_MEDICINE_AND_PRESCRIPTION="pharma.sms.app.newOrder.withPrescription";
	public static final String CONSUMER_ORDER_WITH_ONLY_PRESCRIPTION="pharma.sms.app.newOrder.withOnlyPrescription";
	public static final String CONSUMER_ACTED="pharma.sms.app.acceptOrReject.order";
	public static final String PHARMACY_SEND_OTP="pharma.sms.app.sendOtp";

	
	public static final String CONSUMER_SEND_OTP="pharma.sms.consumer.sendOtp";
	public static final String CONSUMER_PLACES_NEW_ORDER="pharma.sms.consumer.newOrder";
	public static final String PHARMACY_ACCEPTS_ORDER="pharma.sms.consumer.order.accepted";
	public static final String PHARMACY_REJECTS_ORDER="pharma.sms.consumer.order.rejected";
	public static final String PHARMACY_REVISED_ORDER="pharma.sms.consumer.order.revised";
}