package com.itsmypharma.modules.services;

import com.itsmypharma.common.util.CommonUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.HashMap;

@Service
@Slf4j
public class MobileOtpService {
	
	
	@Value("${SMS.gateway}")
	private String OTPUrl;
	
	@Value("${SMS.username}")
	private String username;
	
	@Value("${SMS.password}")
	private String password;


    public String sendMessage(String message, String mobileNumber) {
        HashMap<String, String> requestParams = new HashMap<String, String>();
        requestParams.put("user", username);
        requestParams.put("password", password);
        requestParams.put("sender", "PHYDGI");
        requestParams.put("SMSText", message);
        requestParams.put("GSM", mobileNumber!=null && mobileNumber.length()==10? "91"+mobileNumber:mobileNumber);
        log.info("sending otp for " + mobileNumber!=null && mobileNumber.length()==10? "91"+mobileNumber:mobileNumber);
        return CommonUtil.sendHttpGetRequest(OTPUrl, requestParams);
    }
}
