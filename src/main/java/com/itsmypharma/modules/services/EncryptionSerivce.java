package com.itsmypharma.modules.services;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.security.Key;

@Service
public class EncryptionSerivce {

    private static final String ALGO = "AES";

    @Value("${image.secret.key}")
    private String keyValue;

    public byte[] encrypt(byte[] Data) {
        try {
            Key key = generateKey();
            Cipher c = Cipher.getInstance(ALGO);
            c.init(Cipher.ENCRYPT_MODE, key);
            return c.doFinal(Data);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public byte[] decrypt(byte[] encryptedData) {
        try {
            Key key = generateKey();
            Cipher c = Cipher.getInstance(ALGO);
            c.init(Cipher.DECRYPT_MODE, key);
            return c.doFinal(encryptedData);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private Key generateKey() {
        Key key = new SecretKeySpec(keyValue.getBytes(), ALGO);
        return key;
    }
}
