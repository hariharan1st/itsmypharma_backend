package com.itsmypharma.modules.customer;

import com.itsmypharma.common.generics.GenericEntity;
import lombok.Data;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.io.Serializable;

@Entity
@Table(name = "pharma_customer_mast")
@DynamicInsert
@DynamicUpdate
@Data
public class Customer extends GenericEntity<Integer> implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Id
	@Column(name = "pcm_cust_id")
	private Integer id;

	@Column(name = "pcm_auth_id")
	private Integer userAuthId;
	
	@Column(name = "pcm_first_name")
	private String firstName;
	
	@Column(name = "pcm_last_name")
	private String lastName;

	@Column(name = "pcm_creation_date")
	private Long creationDate;
	
	@Column(name = "pcm_updated_date")
	private Long updatedDate;
	
	@Column(name = "pcm_whatsapp_enabled")
	private Character whatsappEnabled;
	
	@Column(name = "pcm_sms_enabled")
	private Character smsEnabled;


	@Transient
	private String mobileNumber;
}
