package com.itsmypharma.modules.customer;


import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CustomerRepository extends CrudRepository<Customer, Integer> {
    /*@Query("select customer from Customer customer where customer.mobileNo = ?1 and customer.store.id = ?2")
    Customer findByMobileNoAndStore(String mobileNo, Integer storeId);*/

    Customer findByUserAuthId(Integer currentCustomerId);
}
