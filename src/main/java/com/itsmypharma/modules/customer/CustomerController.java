package com.itsmypharma.modules.customer;

import com.itsmypharma.common.generics.GenericRestController;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;

@RestController
@RequestMapping("/api/customer")
@Slf4j
public class CustomerController extends GenericRestController<Customer, CustomerService> {
    @Autowired
    private CustomerService customerService;

    public CustomerController() {
        super(Arrays.asList(HttpMethod.GET, HttpMethod.POST));
    }

    @Override
    public CustomerService getService() {
        return customerService;
    }
}
