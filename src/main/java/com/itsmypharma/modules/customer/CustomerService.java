package com.itsmypharma.modules.customer;

import com.itsmypharma.common.exceptions.ErrorHolder;
import com.itsmypharma.common.generics.GenericEntity;
import com.itsmypharma.common.generics.GenericService;
import com.itsmypharma.common.scope.RequestParamEnum;
import com.itsmypharma.common.util.CommonUtil;
import com.itsmypharma.modules.authentication.UserAuth;
import com.itsmypharma.modules.authentication.UserAuthService;
import com.itsmypharma.modules.services.MobileOtpService;
import com.itsmypharma.modules.store.StoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

@Service
public class CustomerService extends GenericService<Customer> {
    @Autowired
    private CustomerRepository customerRepository;
    @Autowired
    private StoreService storeService;
    @Autowired
    UserAuthService userAuthService;
    @Autowired
    MobileOtpService mobileOtpService;

    public CustomerService(CustomerRepository customerRepository) {
        super(customerRepository, "Customer");
    }

    @Override
    public List<Customer> findAll() {
        if (CommonUtil.isAdminUser()) {
            return super.findAll();
        }
        return Arrays.asList(getCurrentUser());
    }

    public Customer getCurrentUser() {
        Customer customer = (Customer) getRequestParam(RequestParamEnum.CURRENT_CUSTOMER);
        if(customer == null) {
            Integer currentUserAuthId = CommonUtil.getCurrentUserAuthId();
            if(currentUserAuthId != null) {
                customer = customerRepository.findByUserAuthId(currentUserAuthId);
                setRequestParam(RequestParamEnum.CURRENT_CUSTOMER, customer);
            }
        }
        return customer;
    }

    @Override
    public Customer findById(Object id) {
        return getCurrentUser();
    }

    public Customer findByUserAuthId(Integer userAuthId) {
        return customerRepository.findByUserAuthId(userAuthId);
    }

    @Override
    public List<Customer> findAll(Integer offset) {
        return Arrays.asList(getCurrentUser());
    }

    @Override
    public <T extends GenericEntity> void postFieldValidation(T newEntity, ErrorHolder errorHolder) throws ErrorHolder {
        Customer customer = (Customer) newEntity;
        Customer existingCustomer = customerRepository.findByUserAuthId(CommonUtil.getCurrentUserAuthId());
        if(existingCustomer == null) {
            customer.setCreationDate(System.currentTimeMillis());
        } else {
            newEntity.setId(existingCustomer.getId());
        }
        customer.setUpdatedDate(System.currentTimeMillis());
        customer.setUserAuthId(CommonUtil.getCurrentUserAuthId());
        super.postFieldValidation(customer, errorHolder);
    }

    @Override
    protected Customer saveWithoutValidation(Customer entity) {
        Customer customer = getCurrentUser();
        if(customer != null) {
            // to copy properties other than first name, last name, sms and whatsapp enabling
            customer.setFirstName(entity.getFirstName());
            customer.setLastName(entity.getLastName());
            customer.setWhatsappEnabled(entity.getWhatsappEnabled());
            customer.setSmsEnabled(entity.getSmsEnabled());
            return super.saveWithoutValidation(customer);
        }
        return super.saveWithoutValidation(entity);
    }

    public void sendMessage(Customer customer, String message) {
        if(new Character('Y').equals(customer.getSmsEnabled())) {
            Integer userAuthId = customer.getUserAuthId();
            UserAuth storeUser = userAuthService.findById(userAuthId);
            mobileOtpService.sendMessage(message, storeUser.getMobileNumber());
        }
    }
}
