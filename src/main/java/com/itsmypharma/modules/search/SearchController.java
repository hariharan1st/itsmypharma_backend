package com.itsmypharma.modules.search;

import com.itsmypharma.common.util.ResponseData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


@RestController
@CrossOrigin
public class SearchController {
    @Autowired
    SearchService searchService;

    @GetMapping(value = "/api/searchMedicine/{key}")
    public ResponseData searchMedicine(@PathVariable(name="key") String key) {
        return ResponseData.setSuccess(searchService.getSearchItem(key));
    }

    @GetMapping(value = "/api/searchMedicine")
    public ResponseData searchMedicineWithKey(@RequestParam(name="key") String key) {
        return ResponseData.setSuccess(searchService.getSearchItem(key));
    }
    
    @GetMapping(value = "/api/searchPincode/{key}")
    public ResponseData searchPincode(@PathVariable(name="key") String key) {
        return ResponseData.setSuccess(searchService.getCity(key));
    }
}
