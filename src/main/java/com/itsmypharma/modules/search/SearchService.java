package com.itsmypharma.modules.search;

import com.itsmypharma.modules.medicine.MedicalMaster;
import com.itsmypharma.modules.medicine.MedicineResponse;
import com.itsmypharma.modules.medicine.MedicineService;

import lombok.extern.slf4j.Slf4j;

import com.itsmypharma.modules.city.City;
import com.itsmypharma.modules.city.CityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
public class SearchService {

	@Value("${image.location.path}")
	private String imagePath;

	@Autowired
	MedicineService medicineService;

	@Autowired
	CityRepository cityRepository;

	@Transactional
	public List<MedicineResponse> getSearchItem(String key) {
		
		if(key==null)
			return null;

		List<MedicalMaster> masterList = medicineService.getSearchItem(key);
		List<MedicineResponse> medicineList = new ArrayList<>();
		log.info("Goy medicine List...{}",masterList!=null? masterList.size():0);
		for (MedicalMaster master : masterList) {
			medicineList.add(new MedicineResponse(master, imagePath));
		}
		return medicineList;

	}

	@Transactional
	public List<City> getCity(String pincode) {

		pincode = pincode + "%";
		return cityRepository.findByPinCodeLike(pincode);

	}

}
