package com.itsmypharma.modules.medicine;

import com.itsmypharma.common.generics.GenericEntity;
import lombok.Data;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.io.Serializable;

@Entity
@Table(name = "pharma_medi_mast")
@DynamicInsert
@DynamicUpdate
@Data
public class MedicalMaster extends GenericEntity<String> implements Serializable {

	private static final long serialVersionUID = 1L;
	@GeneratedValue(strategy = GenerationType.TABLE)
	@Id
	@Column(name = "pmm_medi_id")
	private String id;

	@Column(name = "pmm_drug_name")
	private String drugName;

	@Column(name = "pmm_composition")
	private String composition;

	@Column(name = "pmm_manufact")
	private String manuFact;

	@Column(name = "pmm_mrp")
	private String mrp;

	@Column(name = "pmm_price")
	private String price;

	@Column(name = "pmm_package_vol")
	private String packageVolume;

	@Column(name = "pmm_presc_type")
	private Character prescType;

	@Column(name = "pmm_medi_use")
	private String mediUse;

	@Column(name = "pmm_how_use")
	private String howToUse;

	@Column(name = "pmm_safety_advice")
	private String safetyAdvice;

	@Column(name = "pmm_alter_brand")
	private String alterBrand;

	@Column(name = "pmm_related_product")
	private String relatedProduct;

	@Column(name = "pmm_manufact_addr")
	private String manuFactAddr;
	
	@Column(name="pmm_image_avail")
	private String isImageAvailable;
	
	@Column(name="pmm_image_path")
	private String fileLocation;

	@Transient
	private Long creationDate;

	@Transient
	private Long updatedDate;
	

//	@OneToMany(mappedBy = "medicalMaster", cascade = CascadeType.ALL, orphanRemoval = true)
//	private List<MedicalImages> imageList;
	
	public MedicalMaster() {
	}
}
