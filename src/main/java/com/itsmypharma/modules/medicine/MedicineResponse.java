package com.itsmypharma.modules.medicine;

import com.itsmypharma.common.util.PharmaUtil;
import lombok.Data;

@Data
public class MedicineResponse {

	public String mediId;

	public String medicineName;

	public String description;

	public String mrp;

	public String price;

	public String company;

	public String companyAddress;

	public String alterBrand;

	public String relatedProduct;

	public String image;
	public String packageVolume;

	public MedicineResponse(MedicalMaster master, String imagePath) {
		this.mediId = master.getId();
		this.medicineName = master.getDrugName();
		this.description = master.getComposition();
		// this.mrp=master.getMrp();
		// this.price=master.getPrice();
		this.company = master.getManuFact();
		this.companyAddress = master.getManuFactAddr();
		// this.alterBrand=master.getAlterBrand();
		// this.relatedProduct=master.getRelatedProduct();
		setImage(PharmaUtil.addImagePath(imagePath, master));
		this.packageVolume = master.getPackageVolume();
	}
}
