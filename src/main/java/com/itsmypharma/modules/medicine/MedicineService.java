package com.itsmypharma.modules.medicine;

import java.util.List;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.itsmypharma.common.generics.GenericService;
import com.itsmypharma.common.util.CommonUtil;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class MedicineService extends GenericService<MedicalMaster> {

	@Autowired
	MedicineRepository medicineRepository;

	public MedicineService(MedicineRepository medicineRepository) {
		super(medicineRepository, "Medicine");
	}

	public List<MedicalMaster> getSearchItem(String drugName) {

		List<MedicalMaster> medicineList = null;
		drugName = drugName.trim();
		if (!drugName.contains(" ") && Pattern.matches("[a-zA-Z0-9]+", drugName)) {
			String drugNameWithSpaceAndNoSpace = ('%' + drugName.replaceAll("[^\\\\.A-Za-z0-9_/]", "") + '%')
					.toUpperCase();
			log.info("Medicine name Input {} for the search Query {}", drugNameWithSpaceAndNoSpace,
					"findDrugNameWhenSplAndNoSpaceAvailable");

			medicineList = medicineRepository.findDrugNameWhenSplAndNoSpaceAvailable(drugNameWithSpaceAndNoSpace,
					CommonUtil.getDefaultPage(0));
		} else if (drugName.contains(" ") && !Pattern.matches("[a-zA-Z0-9]+", drugName)) {
			String drugNameWithSpaceAndSplChar = ('%' + drugName.replaceAll("[^a-zA-Z0-9!@#$&()\\-`.+,/\"]", "") + '%')
					.toUpperCase();
			log.info("Medicine name Input {} for the search Query {}", drugNameWithSpaceAndSplChar,
					"findDrugNameWhenSplAndSpaceAvailable");
			medicineList = medicineRepository.findDrugNameWhenSplAndSpaceAvailable(drugNameWithSpaceAndSplChar,
					CommonUtil.getDefaultPage(0));
		}

		else if (!drugName.contains(" ") && !Pattern.matches("[a-zA-Z0-9]+", drugName)) {
			drugName = '%' + drugName + '%';
			log.info("Medicine name Input {} for the search Query {}", drugName, "findByDrugNameLikeIgnoreCase");
			medicineList = medicineRepository.findByDrugNameLikeIgnoreCase(drugName, CommonUtil.getDefaultPage(0));
		}

		if (medicineList == null) {
			drugName = '%' + drugName + '%';
			log.info("Medicine name Input {} for the search Query {}", drugName, "findByDrugNameLikeIgnoreCase");
			medicineList = medicineRepository.findByDrugNameLikeIgnoreCase(drugName, CommonUtil.getDefaultPage(0));
		}
		return medicineList;
	}

	public MedicalMaster getMedicineById(String id) {
		return medicineRepository.findById(id).orElse(null);
	}

}
