package com.itsmypharma.modules.medicine;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MedicineRepository extends CrudRepository<MedicalMaster, String> {
    List<MedicalMaster> findByDrugNameLikeIgnoreCase(String drugName, Pageable pageable);
    //REGEXP_REPLACE(upper(medi.drugName), '[^a-zA-Z0-9!@#$&()\\\\-`.+,/\\\"]', '')
    @Query("select medi from MedicalMaster medi where REGEXP_REPLACE(upper(medi.drugName), '[^\\\\\\\\.A-Za-z0-9_/]', '') like ?1")
    List<MedicalMaster> findDrugNameWhenSplAndSpaceAvailable(String drugName, Pageable pageable);
    
    @Query("select medi from MedicalMaster medi where REGEXP_REPLACE(upper(medi.drugName), '[^\\\\.A-Za-z0-9_/]', '') like ?1 or upper(medi.drugName) like ?1")
    List<MedicalMaster> findDrugNameWhenSplAndNoSpaceAvailable(String drugName, Pageable pageable);
}
