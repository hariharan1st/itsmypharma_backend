package com.itsmypharma.modules.store;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.itsmypharma.common.generics.GenericEntity;
import com.itsmypharma.common.generics.GenericService;
import com.itsmypharma.modules.city.City;

import lombok.Data;

@Data
@Entity
@Table(name = "pharma_store_mast")
public class Store extends GenericEntity<Integer> {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "psm_id")
	private Integer id;

	@Column(name = "psm_auth_id")
	private Integer userAuthId;

	@Column(name = "psm_registered_pharmacy_name")
	private String registeredPharmacyName;
	@Column(name = "psm_registered_pharmacist_full_name")
	private String pharmacistFullName;
	@Column(name = "psm_drug_license_number")
	private String drugLicenseNumber;
	@Column(name = "psm_address_line_1")
	private String addressLine1;
	@Column(name = "psm_address_line_2")
	private String addressLine2;

	@ManyToOne
	@JoinColumn(name = "psm_city_id")
	private City city;

	@Transient
	@JsonSetter
	private Integer cityId;

	@Column(name = "psm_whatsapp_number")
	private String whatsappNumber;

	@Column(name = "psm_image_logo_id")
	@JsonIgnore
	private Integer imageLogoId;
	@Column(name = "psm_display_name")
	private String displayName;

	@Column(name = "psm_creation_date")
	private Long creationDate;
	@Column(name = "psm_updated_date")
	private Long updatedDate;

	@Column(name = "psm_store_number")
	private String storeNumber;
	// it has two digit state code MH xxxxxx(auto incrementing)

	@Column(name = "psm_order_count")
	private Integer orderCount;

	@Column(name = "psm_sun_start_hour")
	private String sunStartHour;
	@Column(name = "psm_sun_end_hour")
	private String sunEndHour;

	@Column(name = "psm_mon_start_hour")
	private String monStartHour;
	@Column(name = "psm_mon_end_hour")
	private String monEndHour;

	@Column(name = "psm_tue_start_hour")
	private String tueStartHour;
	@Column(name = "psm_tue_end_hour")
	private String tueEndHour;

	@Column(name = "psm_wed_start_hour")
	private String wedStartHour;
	@Column(name = "psm_wed_end_hour")
	private String wedEndHour;

	@Column(name = "psm_thu_start_hour")
	private String thuStartHour;
	@Column(name = "psm_thu_end_hour")
	private String thuEndHour;

	@Column(name = "psm_fri_start_hour")
	private String friStartHour;
	@Column(name = "psm_fri_end_hour")
	private String friEndHour;

	@Column(name = "psm_sat_start_hour")
	private String satStartHour;
	@Column(name = "psm_sat_end_hour")
	private String satEndHour;

	@Transient
	private String imageLogoUrl;

	@Transient
	private String storeUrl;
	
	@Transient
	private String docUrl;
	
	@Column(name = "psm_user_doc_id")
	@JsonIgnore
	private Integer userDocId; 
	
	@Column(name = "psm_month_fee")
	private String monthlyFee;
	
	@Column(name = "psm_annual_fee")
	private String annualFee;
	
	@Column(name="psm_offer_end_date")
	private Date offerDate;
	@Transient
	private String phoneNumber;
	

	@Override
	public void populateTransientValues(GenericService service) {
		if (imageLogoId != null)
			this.imageLogoUrl = service.getApplicationProperty("store.image.url") + imageLogoId;
		if (storeNumber != null)
			this.storeUrl = String.format(service.getApplicationProperty("store.generated_url"), storeNumber);
		this.docUrl=service.getApplicationProperty("store.doc.url")+userDocId;
		
		super.populateTransientValues(service);
	}
}