package com.itsmypharma.modules.store;

import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.LockModeType;
import java.util.Optional;

@Repository
public interface StoreRepository extends CrudRepository<Store, Integer> {
    Store findByDrugLicenseNumber(String drugLicenseNo);
    Store findByUserAuthId(Integer currentStoreId);
    Store findByStoreNumber(String storeNumber);

    @Lock(LockModeType.PESSIMISTIC_WRITE)
    @Query("select store from Store store where store.id = ?1")
    Optional<Store> getStoreForOrderCountUpdate(Integer storeId);
}
