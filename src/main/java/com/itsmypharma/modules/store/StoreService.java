package com.itsmypharma.modules.store;

import java.io.File;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.itsmypharma.common.exceptions.ErrorHolder;
import com.itsmypharma.common.generics.GenericEntity;
import com.itsmypharma.common.generics.GenericService;
import com.itsmypharma.common.scope.RequestParamEnum;
import com.itsmypharma.common.util.CommonUtil;
import com.itsmypharma.common.util.PharmaUtil;
import com.itsmypharma.modules.authentication.UserAuth;
import com.itsmypharma.modules.authentication.UserAuthService;
import com.itsmypharma.modules.city.City;
import com.itsmypharma.modules.city.CityRepository;
import com.itsmypharma.modules.image.ImageService;
import com.itsmypharma.modules.services.MobileOtpService;
import com.itsmypharma.modules.state.State;
import com.itsmypharma.modules.state.StateRepository;
import com.itsmypharma.modules.store.response.StoreResponse;
import com.xandryex.WordReplacer;

@Service
public class StoreService extends GenericService<Store> {
	@Autowired
	StoreRepository storeRepository;
	@Autowired
	UserAuthService userAuthService;
	@Autowired
	ImageService imageService;
	@Autowired
	CityRepository cityRepository;
	@Autowired
	StateRepository stateRepository;
	@Autowired
	MobileOtpService mobileOtpService;

	public StoreService(StoreRepository storeRepository) {
		super(storeRepository, "Store");
	}

	private void populateStoreNumber(Store store, City city) {
		State state = stateRepository.findById(city.getState().getStateId()).orElse(null);
		int storeCount = state.getStoreCount() + 1;
		state.setStoreCount(storeCount);
		stateRepository.save(state);
		String storeNumber = state.getShortCode() + PharmaUtil.padLeft("" + storeCount, 6);
		store.setStoreNumber(storeNumber);
	}

	@Override
	public <T extends GenericEntity> void postFieldValidation(T newEntity, ErrorHolder errorHolder) throws ErrorHolder {
		Store store = (Store) newEntity;
		City city = null;
		Store existingStore = storeRepository.findByUserAuthId(CommonUtil.getCurrentUserAuthId());
		if (existingStore == null) {
			String drugLicenseNo = store.getDrugLicenseNumber();
			Store storeWithDrugLicenseNo = findByDrugLicenseNumber(drugLicenseNo);
			if (storeWithDrugLicenseNo != null) {
				ErrorHolder.throwError("store with given drug license number already exists");
			}
			if (userAuthService.getCurrentUserAuth() == null) {
				ErrorHolder.throwError(CommonUtil.getCurrentUserAuthId() + " - user auth does not exist");
			}
			store.setCreationDate(System.currentTimeMillis());
			city = cityRepository.findById(store.getCityId()).orElse(null);
			store.setOrderCount(0);
			populateStoreNumber(store, city);
		} else {
			if (existingStore == null) {
				ErrorHolder.throwError("Don't pass any store id as user does not have a store yet");
			}
			store.setId(existingStore.getId());
			store.setStoreNumber(existingStore.getStoreNumber());
			store.setOrderCount(existingStore.getOrderCount());
		}

		if (city != null) {
			store.setCity(city);
		} else if (store.getCityId() != null) {
			city = cityRepository.findById(store.getCityId()).orElse(null);
			if (city == null) {
				ErrorHolder.throwError("passed city id does not exists");
			} else {
				store.setCity(city);
			}
		}
		store.setUserAuthId(CommonUtil.getCurrentUserAuthId());
		store.setUpdatedDate(System.currentTimeMillis());
		super.postFieldValidation(newEntity, errorHolder);
	}

	private Store findByDrugLicenseNumber(String drugLicenseNo) {
		if (drugLicenseNo == null) {
			return null;
		}
		return storeRepository.findByDrugLicenseNumber(drugLicenseNo);
	}

	public Store findCurrentStore() {
		return storeRepository.findByUserAuthId(CommonUtil.getCurrentUserAuthId());
	}

	public Store findByUserAuthId(Integer userAuthId) {
		return storeRepository.findByUserAuthId(userAuthId);
	}

	public Integer getCurrentUserStoreId() {
		if (userAuthService.isStoreOwner()) {
			return findCurrentStore().getId();
		} else {
			return userAuthService.getCurrentUserAuth().getStoreId();
		}
	}

	public Store findStoreFromRequest() {
		String requestStoreNumber = (String) getRequestParam(RequestParamEnum.REQUEST_STORE_ID);
		if (requestStoreNumber == null) {
			requestStoreNumber = getHeader("store_id");
		}
		if (requestStoreNumber != null) {
			return storeRepository.findByStoreNumber(requestStoreNumber);
		}
		return null;
	}

	@Override
	public List findAll(Integer offset) {
		if (CommonUtil.isAdminUser()) {
			return super.findAll();
		}
		Store store = findStoreFromRequest();
		if (store != null) {

			UserAuth user = userAuthService.findById(store.getUserAuthId());
			store.populateTransientValues(this);
			StoreResponse responseStore = new StoreResponse();
			CommonUtil.copyNonNullProperties(store, responseStore);
			responseStore.setId(store.getStoreNumber());
			responseStore.setPhoneNumber(user.getMobileNumber());
			return Arrays.asList(responseStore);
		}
		return Arrays.asList(getCurrentUser());
	}

	public Store getCurrentUser() {
		Store store = (Store) getRequestParam(RequestParamEnum.CURRENT_STORE);
		if (store == null) {
			Integer currentCustomerId = CommonUtil.getCurrentUserAuthId();
			if (currentCustomerId != null) {
				store = storeRepository.findByUserAuthId(currentCustomerId);
				UserAuth user = userAuthService.findById(currentCustomerId);
				if (store != null)
					store.populateTransientValues(this);
				store.setPhoneNumber(user.getMobileNumber());
				setRequestParam(RequestParamEnum.CURRENT_STORE, store);
			}
		}
		return store;
	}

	@Override
	public void populateRequestParams(Map<String, String[]> parameterMap, Store requestBody, HttpMethod httpMethod) {
		String storeIdStr = CommonUtil.getRequestParamValue(parameterMap, "store_id");
		if (storeIdStr != null) {
			setRequestParam(RequestParamEnum.REQUEST_STORE_ID, storeIdStr);
		}
		super.populateRequestParams(parameterMap, requestBody, httpMethod);
	}

	public Store createOrUpdate(MultipartFile storeLogoFile, Store store) throws Exception {
		WordReplacer wordReplacer;
		File docxFile = null;
		if (storeLogoFile != null) {
			Integer imageId = imageService.saveImage(storeLogoFile);
			store.setImageLogoId(imageId);
		}
		if (store.getId() == null) {
			City city = cityRepository.findById(store.getCityId()).orElse(null);
			
			InputStream resource=getClass().getClassLoader().getResourceAsStream("User Agreement.docx");
			docxFile=new File("User Agreement.docx");
			FileUtils.copyInputStreamToFile(resource, docxFile);

			
			wordReplacer = new WordReplacer(docxFile);
			wordReplacer.replaceWordsInText("[Insert date]", PharmaUtil.getCurrentDate());
			wordReplacer.replaceWordsInText("[Insert name of the pharmacy],", store.getRegisteredPharmacyName());
			wordReplacer.replaceWordsInText("[company/limited liability partnership/partnership],", "company");
			wordReplacer.replaceWordsInText("[insert address]", store.getAddressLine1() + "," + store.getAddressLine2()
					+ "," + city.getCityName() + "," + city.getState().getStateName() + "," + city.getPinCode());
			wordReplacer.replaceWordsInText("[Insert amount]", "125");
			wordReplacer.replaceWordsInText("[Insert Annual Fee]", "999");
			File modFile = wordReplacer.saveAndGetModdedFile("User Agreement_2020.docx");

			Integer userAgreementId = imageService.saveFile(modFile);
			store.setUserDocId(userAgreementId);
		}
		store.setMonthlyFee("125");
		store.setAnnualFee("999");
		store.setOfferDate(new SimpleDateFormat("dd-MM-yyyy").parse("31-12-2020"));
		return save(store);
	}

	public Store deleteImageLogo() {
		Store store = findByUserAuthId(CommonUtil.getCurrentUserAuthId());
		imageService.delete(store.getImageLogoId());
		store.setImageLogoId(null);
		return save(store);
	}

	public Store findByIdForUpdate(Integer storeId) {
		Store store = storeRepository.getStoreForOrderCountUpdate(storeId).orElse(null);
		if (store != null) {
			Integer storeOrderCount = store.getOrderCount() + 1;
			store.setOrderCount(storeOrderCount);
			return saveWithoutValidation(store);
		}
		return null;
	}

	public void sendMessage(Store store, String message) {
		Integer storeUserAuthId = store.getUserAuthId();
		UserAuth storeUser = userAuthService.findById(storeUserAuthId);
		mobileOtpService.sendMessage(message, storeUser.getMobileNumber());
	}
}
