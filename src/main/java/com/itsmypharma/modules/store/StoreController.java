package com.itsmypharma.modules.store;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.itsmypharma.common.util.ResponseData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.List;


@RestController
@RequestMapping("/api/store")
public class StoreController {
    @Autowired
    StoreService storeService;
    @Autowired
    HttpServletRequest request;

    @RequestMapping(value = "/set", produces = MediaType.APPLICATION_JSON_UTF8_VALUE, method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    ResponseData postEntity(@RequestParam(value = "image", required = false) MultipartFile storeLogoFile, @RequestParam("store") String storeRequest) throws Throwable {
        ObjectMapper mapper = new ObjectMapper();
        Store store = mapper.readValue(storeRequest, Store.class);
        storeService.populateRequestParams(request.getParameterMap(), store, HttpMethod.POST);
        store = storeService.createOrUpdate(storeLogoFile, store);
        return ResponseData.setSuccess(store);
    }

    @RequestMapping(value = "/delete_logo", produces = MediaType.APPLICATION_JSON_UTF8_VALUE, method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    ResponseData deleteImage() throws Throwable {
        Store store = storeService.deleteImageLogo();
        return ResponseData.setSuccess(store);
    }

    @Transactional(readOnly = true)
    @RequestMapping(value = "/get", produces = MediaType.APPLICATION_JSON_UTF8_VALUE, method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    ResponseData getEntities(@RequestParam(required = false) String id, @RequestParam(required = false) Integer offset) throws Throwable {
        storeService.populateRequestParams(request.getParameterMap(), null, HttpMethod.GET);
        if (id != null) {
            Store store = storeService.findById(id);
            return ResponseData.setSuccess(store);
        } else {
            if(offset == null) {
                offset = 0;
            }
            List entityList = storeService.findAll(offset);
            if(entityList.size() == 1) {
                return ResponseData.setSuccess(entityList.get(0));
            } else {
                return ResponseData.setSuccess(entityList);
            }
        }
    }
}
