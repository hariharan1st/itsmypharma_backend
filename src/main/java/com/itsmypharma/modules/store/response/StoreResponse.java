package com.itsmypharma.modules.store.response;

import java.util.Date;

import com.itsmypharma.modules.city.City;

import lombok.Data;

@Data
public class StoreResponse {
	private String phoneNumber;
	private String displayName;
	private String id;
	private String addressLine1;
	private String addressLine2;
	private String imageLogoUrl;
	private City city;
	private String whatsappNumber;
	private String registeredPharmacyName;

	private String sunStartHour;
	private String sunEndHour;

	private String monStartHour;
	private String monEndHour;

	private String tueStartHour;
	private String tueEndHour;

	private String wedStartHour;
	private String wedEndHour;

	private String thuStartHour;
	private String thuEndHour;

	private String friStartHour;
	private String friEndHour;

	private String satStartHour;
	private String satEndHour;

	private String monthlyFee;
	private String docUrl;
	private String annualFee;
	private Date offerDate;
	private Long creationDate; 

}
