package com.itsmypharma.modules.state;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.itsmypharma.modules.city.City;
import lombok.Data;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "pharma_state_mast")
@DynamicInsert
@DynamicUpdate
@Data
public class State implements Serializable{

	private static final long serialVersionUID = 1L;
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Id
	@Column(name = "psm_state_id")
	private Integer stateId;

	@Column(name = "psm_state_name")
	private String stateName;

	@Column(name = "psm_short_code")
	private String shortCode;

	@Column(name = "psm_store_count")
	@JsonIgnore
	private Integer storeCount;

	@JsonIgnore
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "state", cascade = CascadeType.ALL)
	private List<City> cityList;

	@Override
	public String toString() {
		return "State{" +
				"stateId=" + stateId +
				", stateName='" + stateName + '\'' +
				", shortCode='" + shortCode + '\'' +
				", storeCount=" + storeCount +
				'}';
	}
}
