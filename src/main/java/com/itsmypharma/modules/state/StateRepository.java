package com.itsmypharma.modules.state;

import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.LockModeType;
import java.util.Optional;

@Repository
public interface StateRepository extends CrudRepository<State, Integer> {
    @Lock(LockModeType.PESSIMISTIC_WRITE)
    @Override
    Optional<State> findById(Integer integer);
}
