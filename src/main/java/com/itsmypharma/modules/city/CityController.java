package com.itsmypharma.modules.city;

import com.itsmypharma.common.generics.GenericRestController;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;

@RestController
@RequestMapping("/api/city")
@Slf4j
public class CityController extends GenericRestController<City, CityService> {

    public CityController() {
        super(Arrays.asList(HttpMethod.GET));
    }

    @Autowired
    CityService cityService;

    @Override
    protected CityService getService() {
        return cityService;
    }
}
