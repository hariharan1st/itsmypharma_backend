package com.itsmypharma.modules.city;

import com.itsmypharma.common.generics.GenericService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CityService extends GenericService<City> {

    @Autowired
    CityRepository cityRepository;

    public CityService(CityRepository cityRepository) {
        super(cityRepository, "City");
    }
}
