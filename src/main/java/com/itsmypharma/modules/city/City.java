package com.itsmypharma.modules.city;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.itsmypharma.common.generics.GenericEntity;
import com.itsmypharma.modules.state.State;
import lombok.Data;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.io.Serializable;

@Entity
@Table(name = "pharma_city_mast")
@DynamicInsert
@DynamicUpdate
@Data
public class City extends GenericEntity<Integer> implements Serializable {

	private static final long serialVersionUID = 1L;
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Id
	@Column(name = "pcm_city_id")
	private Integer id;

	@Column(name = "pcm_city_name")
	private String cityName;

	@Column(name = "pcm_pin_code")
	private String pinCode;

	@ManyToOne
	@JoinColumn(name = "pcm_state_id")
	private State state;

	@Transient
	@JsonIgnore
	private Long creationDate;

	@Transient
	@JsonIgnore
	private Long updatedDate;

	@Override
	public String toString() {
		return "City{" +
				"cityId=" + id +
				", cityName='" + cityName + '\'' +
				", pinCode='" + pinCode + '\'' +
				'}';
	}
}
