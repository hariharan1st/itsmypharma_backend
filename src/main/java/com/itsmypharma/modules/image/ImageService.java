package com.itsmypharma.modules.image;

import java.io.File;
import java.nio.file.Files;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.itsmypharma.common.generics.GenericService;
import com.itsmypharma.modules.services.EncryptionSerivce;

@Service
public class ImageService extends GenericService<Image> {

    @Autowired
    ImageRepo imageRepo;
    @Autowired
    EncryptionSerivce encryptionSerivce;

    public ImageService(ImageRepo imageRepo) {
        super(imageRepo, "Image");
    }

    public Image findByIdForImageDownload(Integer id) {
        Image image = imageRepo.findById(id).orElse(null);
        if(image != null) {
            image.setDecryptedImageData(encryptionSerivce.decrypt(image.getImageData()));
        }
        return image;
    }

    public Image findById(Integer id) {
        return imageRepo.findById(id).orElse(null);
    }
  
    public Integer saveFile(File imageFile) {
        Image image = new Image();
        image.setMediaType("application/octet-stream");
        image.setFileName(imageFile.getName());
        System.out.println(imageFile.length());
        try {
            byte[] bytes = Files.readAllBytes(imageFile.toPath());
            image.setFileSize(Long.valueOf(bytes.length));
            image.setImageData(encryptionSerivce.encrypt(bytes));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        super.save(image);
        return image.getId();
    }
    

    public Integer saveImage(MultipartFile imageFile) {
        Image image = new Image();
        image.setMediaType(imageFile.getContentType());
        image.setFileName(imageFile.getOriginalFilename());
        image.setFileSize(imageFile.getSize());
        System.out.println(imageFile.getSize());
        try {
            byte[] bytes = imageFile.getBytes();
            image.setImageData(encryptionSerivce.encrypt(bytes));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        super.save(image);
        return image.getId();
    }
}
