package com.itsmypharma.modules.image;

import com.itsmypharma.common.generics.GenericEntity;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Data
@Entity
@Table(name = "pharma_image_mast")
public class Image extends GenericEntity<Integer> {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "pim_id")
    private Integer id;

    @Column(name = "pim_image")
    private byte[] imageData;

    @Transient
    private byte[] decryptedImageData;

    @Column(name = "pim_file_type")
    private String mediaType;

    @Column(name = "pim_file_name")
    private String fileName;

    @Column(name = "pim_size")
    private Long fileSize;

    @Column(name = "pim_creation_date")
    private Long creationDate;

    @Column(name = "pim_updated_date")
    private Long updatedDate;
}
