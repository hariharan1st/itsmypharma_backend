package com.itsmypharma.modules.image;

import java.io.IOException;
import java.util.Optional;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.itsmypharma.common.exceptions.ErrorHolder;

@RestController
@RequestMapping("/api/image")
public class ImageController {

	@Autowired
	ImageService imageService;

	@GetMapping
	public ResponseEntity<byte[]> getImage(@RequestParam("id") Integer id) {
		Image image = imageService.findByIdForImageDownload(id);
		if (image != null) {
			MediaType mediaType = MediaType.parseMediaType(image.getMediaType());
			return ResponseEntity.ok().contentType(mediaType).body(image.getDecryptedImageData());
		} else {
			return ResponseEntity.notFound().build();
		}
	}

	@GetMapping("prescription")
	public ResponseEntity<byte[]> getPrescription(@RequestParam("id") Integer id) {
		// handle security for all the images based on the login user id
		return getImage(id);
	}

	@GetMapping("store-logo")
	public ResponseEntity<byte[]> getStoreLogo(@RequestParam("id") Integer id) {
		// handle security for all the images based on the login user id
		return getImage(id);
	}

	@GetMapping("userdoc")
	public void getUserDoc(@RequestParam("id") Integer id, HttpServletResponse response) throws IOException {
		// handle security for all the images based on the login user id
		Image image = imageService.findByIdForImageDownload(id);
		if (image != null) {
			String headerKey="Content-Disposition";
			String headerValue="attachment; filename="+image.getFileName();
			response.setHeader(headerKey, headerValue);
			
			ServletOutputStream out=response.getOutputStream();
			out.write(image.getDecryptedImageData());
			out.close();

		} else {
			ErrorHolder.throwError("Couldn't reterive the document");
		}

	}
}
