package com.itsmypharma.modules.order.request;

import lombok.Data;

@Data
public class OrderDetailsRequest {
    Integer id;
    String mediId;
    Integer quantity;
    String alternateMediId;
    Integer status;
}
