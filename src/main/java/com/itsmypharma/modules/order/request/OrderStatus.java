package com.itsmypharma.modules.order.request;

public enum OrderStatus {
    CUSTOMER_PLACES_ORDER(1),
    PHARMACY_ACCEPTS_ORDER(2),
    PHARMACY_REJECTS_ORDER(3),
    PHARMACY_EDITS_ORDER(4),
    CUSTOMER_ACCEPTS_REVISION(5),
    CUSTOMER_REJECTS_REVISION(6),
    CUSTOMER_CANCELS_BEFORE_PICKUP(7),
    CUSTOMER_CANCELS_IN_STORE(8),
    CUSTOMER_CANCELS_BEFORE_PHARMACY_ACCEPTS_OR_REJECTS(9),
    CUSTOMER_NOT_ACCEPTED_TILL_PICKUP_TIME(10),
    PHARMACY_NOT_ACCEPTED_TILL_PICKUP_TIME(11),
    PHARMACY_DELIVERED(12),
    ORDER_NEW(1),
    ORDER_ACCEPT(2),
    ORDER_REJECT(3),
    ORDER_COMPLETED(4),
    FILTER_NEW_ORDERS(1),
    FILTER_PENDING_ORDERS(2),
    FILTER_COMPLETED_ORDERS(3);

    private Integer value;

    OrderStatus(Integer value) {
            this.value = value;
        }

    public Integer getValue() {
        return value;
    }

    public static OrderStatus getEnum(Integer value) {
        for(OrderStatus orderStatus: values()) {
            if(orderStatus.value == value) {
                return orderStatus;
            }
        }
        return null;
    }
}
