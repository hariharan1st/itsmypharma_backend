package com.itsmypharma.modules.order.request;

import lombok.Data;

import java.util.List;

@Data
public class OrderRequest {
    Long deliveryTime;
    String orderNumber;
    String comments;
    Double estimatedPrice;
    Integer status;
    String billNumber;
    List<OrderDetailsRequest> orderDetails;
}
