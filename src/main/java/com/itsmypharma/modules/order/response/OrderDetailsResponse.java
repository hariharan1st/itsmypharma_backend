package com.itsmypharma.modules.order.response;

import com.itsmypharma.modules.medicine.MedicineResponse;
import com.itsmypharma.modules.order.OrderDetails;
import lombok.Data;

@Data
public class OrderDetailsResponse {
    private Integer id;

    private MedicineResponse medicine;

    private Integer quantity;

    private Integer prescriptionId;

    private MedicineResponse alternateMedicine;


    private Long creationDate;

    private Long updatedDate;

    public OrderDetailsResponse(OrderDetails orderDetails, String imagePath) {
        this.id = orderDetails.getId();
        if(orderDetails.getMedicine() != null) {
            this.medicine = new MedicineResponse(orderDetails.getMedicine(), imagePath);
        }
        this.quantity = orderDetails.getQuantity();
        this.prescriptionId = orderDetails.getPrescriptionId();
        if(orderDetails.getAlternateMedicine() != null) {
            this.alternateMedicine = new MedicineResponse(orderDetails.getAlternateMedicine(), imagePath);
        }
        this.creationDate = orderDetails.getCreationDate();
        this.updatedDate = orderDetails.getUpdatedDate();
    }
}
