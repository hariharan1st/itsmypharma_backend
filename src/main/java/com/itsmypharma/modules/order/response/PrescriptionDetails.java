package com.itsmypharma.modules.order.response;

import lombok.Data;

@Data
public class PrescriptionDetails {
    private Integer prescriptionId;
    private String prescriptionImageUrl;
    private String fileName;
}
