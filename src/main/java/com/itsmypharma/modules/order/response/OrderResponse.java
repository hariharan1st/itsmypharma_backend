package com.itsmypharma.modules.order.response;

import com.itsmypharma.modules.customer.Customer;
import com.itsmypharma.modules.store.Store;
import lombok.Data;

import java.util.List;

@Data
public class OrderResponse {
    private List<PrescriptionDetails> prescriptionDetails;

    private String pickupStatus;

    private Integer id;

    private Customer customer;

    private Store store;

    private String orderNumber;

    private Long deliveryTime;

    private Integer status;

    private Double estimatedPrice;

    private List<OrderDetailsResponse> orderDetailsList;

    private Long creationDate;
    private Long updatedDate;
    private String comments;
    
    private String billNumber;

    public OrderResponse() {}
}
