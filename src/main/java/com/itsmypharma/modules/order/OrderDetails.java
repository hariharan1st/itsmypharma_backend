package com.itsmypharma.modules.order;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.itsmypharma.common.generics.GenericEntity;
import com.itsmypharma.modules.medicine.MedicalMaster;
import lombok.Data;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "pharma_order_details")
@DynamicInsert
@DynamicUpdate
@Data
public class OrderDetails extends GenericEntity<Integer> implements Serializable {

    private static final long serialVersionUID = 1L;
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "pod_order_details_id")
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "pod_order_id")
    @JsonIgnore
    private Order order;

    @ManyToOne
    @JoinColumn(name = "pod_medi_id")
    private MedicalMaster medicine;

    @Column(name = "pod_quantity")
    private Integer quantity;

    @Column(name = "pod_image_id")
    private Integer prescriptionId;

    @ManyToOne
    @JoinColumn(name = "pod_altr_medi_id")
    private MedicalMaster alternateMedicine;


    @Column(name = "pod_creation_date")
    private Long creationDate;

    @Column(name = "pod_updated_date")
    private Long updatedDate;

    public OrderDetails() {
    }

    public OrderDetails(Order order, MedicalMaster medicineById, Integer quantity, MedicalMaster alternateMedicine) {
        this(order, medicineById, quantity);
        this.alternateMedicine = alternateMedicine;
    }

    public OrderDetails(Order order, Integer prescriptionId) {
        this.order = order;
        this.prescriptionId = prescriptionId;
    }

    public OrderDetails(Order order, MedicalMaster medicine, Integer quantity) {
        this.order = order;
        this.medicine = medicine;
        this.quantity = quantity;
    }

    @Override
    public String toString() {
        return "OrderDetails{" +
                "id=" + id +
                ", medicine=" + medicine +
                ", quantity=" + quantity +
                ", prescriptionId=" + prescriptionId +
                ", alternateMedicine=" + alternateMedicine +
                ", creationDate=" + creationDate +
                ", updatedDate=" + updatedDate +
                '}';
    }
}
