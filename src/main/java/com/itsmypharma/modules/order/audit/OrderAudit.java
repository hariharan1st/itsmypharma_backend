package com.itsmypharma.modules.order.audit;

import lombok.Data;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "pharma_order_audit")
@DynamicInsert
@DynamicUpdate
public class OrderAudit {

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "poa_order_audit_id")
    private Integer id;

    @Column(name = "poa_order_id")
    private Integer orderId;

    @Column(name = "poa_customer_id")
    private Integer customer;

    @Column(name = "poa_store_id")
    private Integer store;

    @Column(name = "poa_order_number")
    private String orderNumber;

    @Column(name = "poa_delivery_time")
    private Long deliveryTime;

    @Column(name = "poa_status")
    private Integer status;

    @Column(name = "poa_estimated_price")
    private Double estimatedPrice;

    @Column(name = "poa_timestamp")
    private Long timestamp;

    @Column(name = "poa_comments")
    private String comments;

    @Column(name = "poa_created_by")
    private Integer createdBy;

    @Column(name = "poa_operation")
    private Integer operation;

    @Column(name = "poa_bill_no")
    private String billNumber;

}
