package com.itsmypharma.modules.order.audit;

import com.itsmypharma.common.util.CommonUtil;
import com.itsmypharma.modules.medicine.MedicalMaster;
import com.itsmypharma.modules.order.OrderDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OrderDetailAuditService {

    @Autowired
    OrderDetailAuditRepository orderDetailAuditRepository;

    public void auditOrderDetail(OrderDetails orderDetails, Integer operation) {
        OrderDetailAudit orderDetailAudit = new OrderDetailAudit();
        orderDetailAudit.setOrderDetail(orderDetails.getId());
        MedicalMaster alternateMedicine = orderDetails.getAlternateMedicine();
        if(alternateMedicine != null) {
            orderDetailAudit.setAlternateMedicine(alternateMedicine.getId());
        }
        MedicalMaster medicine = orderDetails.getMedicine();
        if(medicine != null) {
            orderDetailAudit.setMedicine(medicine.getId());
        }
        orderDetailAudit.setOrder(orderDetails.getOrder().getId());
        orderDetailAudit.setQuantity(orderDetails.getQuantity());
        orderDetailAudit.setPrescriptionId(orderDetails.getPrescriptionId());
        orderDetailAudit.setCreatedBy(CommonUtil.getCurrentUserAuthId());
        orderDetailAudit.setTimestamp(System.currentTimeMillis());
        orderDetailAudit.setOperation(operation);
        orderDetailAuditRepository.save(orderDetailAudit);
    }
}
