package com.itsmypharma.modules.order.audit;

import com.itsmypharma.common.util.CommonUtil;
import com.itsmypharma.modules.order.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OrderAuditService {

    @Autowired
    OrderAuditRepository orderAuditRepository;

    public void auditOrder(Order order, Integer operation) {
        OrderAudit orderAudit = new OrderAudit();
        orderAudit.setComments(order.getComments());
        orderAudit.setCreatedBy(CommonUtil.getCurrentUserAuthId());
        orderAudit.setCustomer(order.getCustomer().getId());
        orderAudit.setTimestamp(System.currentTimeMillis());
        orderAudit.setStatus(order.getStatus());
        orderAudit.setOrderNumber(order.getOrderNumber());
        orderAudit.setDeliveryTime(order.getDeliveryTime());
        orderAudit.setEstimatedPrice(order.getEstimatedPrice());
        orderAudit.setStore(order.getStore().getId());
        orderAudit.setOrderId(order.getId());
        orderAudit.setOperation(operation);
        orderAudit.setBillNumber(order.getBillNumber());
        orderAuditRepository.save(orderAudit);
    }

}
