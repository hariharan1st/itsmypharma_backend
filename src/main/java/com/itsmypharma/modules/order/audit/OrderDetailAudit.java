package com.itsmypharma.modules.order.audit;

import lombok.Data;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Data
@Entity
@Table(name = "pharma_order_detail_audit")
@DynamicInsert
@DynamicUpdate
public class OrderDetailAudit {

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "poda_id")
    private Integer id;

    @Column(name = "poda_order_details_id")
    private Integer orderDetail;

    @Column(name = "poda_order_id")
    private Integer order;

    @Column(name = "poda_medi_id")
    private String medicine;

    @Column(name = "poda_quantity")
    private Integer quantity;

    @Column(name = "poda_image_id")
    private Integer prescriptionId;

    @Column(name = "poda_altr_medi_id")
    private String alternateMedicine;

    @Column(name = "poda_created_by")
    private Integer createdBy;

    @Column(name = "poda_timestamp")
    private Long timestamp;

    @Column(name = "poda_operation")
    private Integer operation;
}
