package com.itsmypharma.modules.order.audit;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderAuditRepository extends CrudRepository<OrderAudit, Integer> {
}
