package com.itsmypharma.modules.order.details;

import com.itsmypharma.common.audit.EntityState;
import com.itsmypharma.common.generics.GenericService;
import com.itsmypharma.modules.order.Order;
import com.itsmypharma.modules.order.OrderDetails;
import com.itsmypharma.modules.order.audit.OrderDetailAuditService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OrderDetailsService extends GenericService<OrderDetails> {

    @Autowired
    OrderDetailsRepository orderDetailsRepository;
    @Autowired
    OrderDetailAuditService orderDetailAuditService;

    public OrderDetailsService(OrderDetailsRepository orderDetailsRepository) {
        super(orderDetailsRepository, "order details");
    }

    public void delete(Order order, Integer id) {
        OrderDetails orderDetails = findById(id);
        order.getOrderDetailsList().remove(orderDetails);
        delete(orderDetails);
    }

    @Override
    protected void postSave(OrderDetails orderDetails, EntityState entityState) {
        orderDetailAuditService.auditOrderDetail(orderDetails, entityState.getValue());
        super.postSave(orderDetails, entityState);
    }
}
