package com.itsmypharma.modules.order.details;

public class OrderDetailStatus {
    public static final Integer ADDED = 1;
    public static final Integer UPDATED = 2;
    public static final Integer DELETED = 3;
}
