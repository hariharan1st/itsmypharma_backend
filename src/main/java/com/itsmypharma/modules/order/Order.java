package com.itsmypharma.modules.order;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.itsmypharma.common.generics.GenericEntity;
import com.itsmypharma.modules.customer.Customer;
import com.itsmypharma.modules.store.Store;
import lombok.Data;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "pharma_order_mast")
@DynamicInsert
@DynamicUpdate
@Data
public class Order extends GenericEntity<Integer> implements Serializable {

	private static final long serialVersionUID = 1L;
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Id
	@Column(name = "pom_order_id")
	private Integer id;

	@OneToOne
	@JoinColumn(name = "pom_customer_id")
	private Customer customer;

	@OneToOne
	@JoinColumn(name = "pom_store_id")
	@JsonIgnore
	private Store store;

	@Column(name = "pom_order_number")
	private String orderNumber;

	@Column(name = "pom_delivery_time")
	private Long deliveryTime;

	@Column(name = "pom_status")
	private Integer status;

	@Column(name = "pom_estimated_price")
	private Double estimatedPrice;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "order", cascade = CascadeType.ALL)
	private List<OrderDetails> orderDetailsList;

	@Column(name = "pom_creation_date")
	private Long creationDate;

	@Column(name = "pom_updated_date")
	private Long updatedDate;

	@Column(name = "pom_comments")
	private String comments;

	@Column(name = "pom_bill_no")
	private String billNumber;

	@Transient
	private boolean medicineAvailable = false;
	@Transient
	private boolean prescAvailable = false;

	public Order() {
	}
}
