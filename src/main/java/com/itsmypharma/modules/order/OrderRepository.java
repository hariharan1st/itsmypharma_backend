package com.itsmypharma.modules.order;


import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrderRepository extends CrudRepository<Order, Long> {

    @Query("select count(*) from Order order where order.customer.id = ?1 and order.status in ?2")
    Integer orderTotalPagesForCustomer(Integer customerId, List<Integer> status);

    @Query("select order from Order order where order.customer.id = ?1")
    List<Order> findByCustomer(Integer customerId, Pageable pageRequest);

    @Query("select count(*) from Order order where order.store.id = ?1 and order.status in ?2")
    Integer orderTotalPagesForStore(Integer storeId, List<Integer> status);

    @Query("select order from Order order where order.store.id = ?1 and order.status in ?2")
    List<Order> findByStore(Integer storeId, List<Integer> status, Pageable pageRequest);

    @Query("select order from Order order where order.orderNumber = ?1 and order.store.id in ?2")
    Order findByOrderNumber(String orderNumber, Integer storeId);

    @Query("select order from Order order where order.store.id = ?1 and order.orderNumber = ?2")
    Order findByOrderNumberAndStore(Integer storeId, String orderNumber);

    @Query("select order from Order order, Customer customer, UserAuth user_auth" +
            " where order.customer.id = customer.id and customer.userAuthId = user_auth.id " +
            "and order.store.id = ?1 and user_auth.mobileNumber = ?2")
    List<Order> getByMobileNumberAndStore(Integer storeId, String mobileNumber);
}