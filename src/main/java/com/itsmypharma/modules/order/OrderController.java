package com.itsmypharma.modules.order;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.itsmypharma.common.generics.GenericRestController;
import com.itsmypharma.common.util.ResponseData;
import com.itsmypharma.modules.customer.Customer;
import com.itsmypharma.modules.order.request.OrderRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/order")
@Slf4j
public class OrderController extends GenericRestController<Customer, OrderService> {
    @Autowired
    private OrderService orderService;

    public OrderController() {
        super(Arrays.asList(HttpMethod.GET));
    }

    @Override
    public OrderService getService() {
        return orderService;
    }

    @Transactional(readOnly = true)
    @RequestMapping(value = "/get_order_list", produces = MediaType.APPLICATION_JSON_UTF8_VALUE, method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    ResponseData getOrderList(@RequestParam(required = false) Integer offset,
                              @RequestParam(value="searchTerm", required = false) String searchTerm,
                              @RequestParam(value = "status", required = false) Integer orderStatus,
                              @RequestParam(value = "creation_date_sort", required = false) Integer sortByCreationDate,
                              @RequestParam(value = "pickup_date_sort", required = false) Integer sortByPickUpDate) throws Throwable {
        List ordersWithoutChildren = null;
        if(searchTerm == null) {
            ordersWithoutChildren = orderService.findOrdersWithoutChildren(offset, orderStatus, sortByCreationDate, sortByPickUpDate, false);
        } else {
            ordersWithoutChildren = orderService.findOrderBySearchTerm(searchTerm);
        }
        return ResponseData.setSuccess(ordersWithoutChildren, orderService.getTotalPages());
    }

    @Transactional(readOnly = true)
    @RequestMapping(value = "/get_order", produces = MediaType.APPLICATION_JSON_UTF8_VALUE, method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    ResponseData getOrder(@RequestParam("order_number") String orderNumber) throws Throwable {
        return ResponseData.setSuccess(orderService.getOrder(orderNumber));
    }

    @RequestMapping(value = "/create_order", produces = MediaType.APPLICATION_JSON_UTF8_VALUE, method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.OK)
    ResponseData createOrder(@RequestBody OrderRequest orderRequest) throws Throwable {
        String newOrder = orderService.createNewOrder(null, orderRequest);
        return ResponseData.setSuccess(newOrder);
    }

    @RequestMapping(value = "/update_order", produces = MediaType.APPLICATION_JSON_UTF8_VALUE, method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.OK)
    ResponseData updateOrder(@RequestBody OrderRequest orderRequest) throws Throwable {
        Order updatedOrder = orderService.updateOrder(orderRequest);
        return ResponseData.setSuccess(updatedOrder);
    }

    @RequestMapping(value = "/order_status_update", produces = MediaType.APPLICATION_JSON_UTF8_VALUE, method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.OK)
    ResponseData updateOrder(@RequestBody Map requestBody) throws Throwable {
        String orderNumber = (String) requestBody.get("orderNumber");
        String billNumber = (String) requestBody.get("billNumber");
        Integer orderStatus = (Integer) requestBody.get("orderStatus");
        Order updatedOrder = orderService.updateOrderStatus(orderNumber, orderStatus, billNumber);
        return ResponseData.setSuccess(updatedOrder);
    }

    @RequestMapping(value = "/create_order_with_prescription", produces = MediaType.APPLICATION_JSON_UTF8_VALUE, method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.OK)
    ResponseData prescriptionUpload(@RequestParam("images") MultipartFile[] files, @RequestParam("order") String requestOrder) throws Throwable {
        ObjectMapper mapper = new ObjectMapper();
        OrderRequest orderRequest = mapper.readValue(requestOrder, OrderRequest.class);
        String newOrder = orderService.createNewOrder(files, orderRequest);
        return ResponseData.setSuccess(newOrder);
    }
}
