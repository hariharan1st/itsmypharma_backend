package com.itsmypharma.modules.order;

import com.itsmypharma.common.audit.EntityState;
import com.itsmypharma.common.exceptions.ErrorHolder;
import com.itsmypharma.common.generics.GenericService;
import com.itsmypharma.common.scope.RequestParamEnum;
import com.itsmypharma.common.util.CommonUtil;
import com.itsmypharma.common.util.CopyUtil;
import com.itsmypharma.common.util.PharmaUtil;
import com.itsmypharma.constants.Constants;
import com.itsmypharma.constants.PharmaResourceBundle;
import com.itsmypharma.constants.ResourceBundleConstants;
import com.itsmypharma.modules.authentication.UserAuth;
import com.itsmypharma.modules.authentication.UserAuthService;
import com.itsmypharma.modules.customer.Customer;
import com.itsmypharma.modules.customer.CustomerService;
import com.itsmypharma.modules.image.Image;
import com.itsmypharma.modules.image.ImageService;
import com.itsmypharma.modules.medicine.MedicalMaster;
import com.itsmypharma.modules.medicine.MedicineService;
import com.itsmypharma.modules.order.audit.OrderAuditService;
import com.itsmypharma.modules.order.details.OrderDetailStatus;
import com.itsmypharma.modules.order.details.OrderDetailsService;
import com.itsmypharma.modules.order.request.OrderDetailsRequest;
import com.itsmypharma.modules.order.request.OrderRequest;
import com.itsmypharma.modules.order.request.OrderStatus;
import com.itsmypharma.modules.order.response.OrderDetailsResponse;
import com.itsmypharma.modules.order.response.OrderResponse;
import com.itsmypharma.modules.order.response.PrescriptionDetails;
import com.itsmypharma.modules.store.Store;
import com.itsmypharma.modules.store.StoreService;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
public class OrderService extends GenericService<Order> {
	@Autowired
	OrderRepository orderRepository;
	@Autowired
	OrderDetailsService orderDetailsService;
	@Autowired
	CustomerService customerService;
	@Autowired
	StoreService storeService;
	@Autowired
	UserAuthService userAuthService;
	@Autowired
	MedicineService medicineService;
	@Autowired
	ImageService imageService;
	@Autowired
	OrderAuditService orderAuditService;

	@Value("${image.location.path}")
	private String medicineImagePath;

	public OrderService(OrderRepository orderRepository) {
		super(orderRepository, "Order");
	}

	public Order createNewOrder(Long deliveryTime, String comments, String billNumber, boolean isMedicineAvailable,
			boolean isPresAvailable) {
		Order order = new Order();
		order.setCustomer(customerService.getCurrentUser());
		Integer storeId = userAuthService.getCurrentUserAuth().getStoreId();
		Store store = storeService.findByIdForUpdate(storeId);
		order.setStore(store);
		order.setStatus(OrderStatus.CUSTOMER_PLACES_ORDER.getValue());
		order.setDeliveryTime(deliveryTime);
		order.setComments(comments);
		order.setBillNumber(billNumber);
		order.setOrderNumber("" + PharmaUtil.padLeft("" + store.getOrderCount(), 6));
		return save(order);
	}

	public String createNewOrder(MultipartFile[] files, OrderRequest orderRequest) {

		List<OrderDetailsRequest> orderDetailsList = orderRequest.getOrderDetails();
		boolean isMedicineAvailable = orderDetailsList != null && orderDetailsList.size() != 0;
		boolean isPresAvailable = files != null && files.length != 0;
		Order order = createNewOrder(orderRequest.getDeliveryTime(), orderRequest.getComments(),
				orderRequest.getBillNumber(), isMedicineAvailable, isPresAvailable);

		List<OrderDetails> orderDetailsToBeSavedList = new ArrayList<>();
		for (OrderDetailsRequest orderDetails : orderDetailsList) {
			String mediId = orderDetails.getMediId();
			MedicalMaster medicineById = medicineService.getMedicineById(mediId);
			if (medicineById == null) {
				ErrorHolder.throwError("medicine not found ::: " + mediId);
			}
			Integer quantity = orderDetails.getQuantity();
			if (quantity == null || quantity <= 0) {
				ErrorHolder.throwError("quantity cannot be null or less than or equal to 0");
			}
			OrderDetails orderDetail = new OrderDetails(order, medicineById, quantity, null);
			orderDetailsToBeSavedList.add(orderDetail);

		}
		if (files != null) {
			for (MultipartFile file : files) {
				Integer imageId = imageService.saveImage(file);
				if (imageId == null) {
					ErrorHolder.throwError("Error saving the image.. please retry");
				}
				OrderDetails orderDetail = new OrderDetails(order, imageId);
				orderDetailsToBeSavedList.add(orderDetail);

			}
		}
		orderDetailsService.save(orderDetailsToBeSavedList);

		return order.getOrderNumber();
	}

	@Override
	public List<Order> findAll(Integer offset) {
		List<Order> orderList;
		if (userAuthService.isStoreOwner()) {
			Integer storeId = storeService.getCurrentUser().getId();
			setRequestParam(RequestParamEnum.ORDER_TOTAL_PAGES,
					orderRepository.orderTotalPagesForStore(storeId, Arrays.asList()));
			orderList = orderRepository.findByStore(storeId, Arrays.asList(), CommonUtil.getDefaultPage(offset));
		} else {
			Integer customerId = customerService.getCurrentUser().getId();
			setRequestParam(RequestParamEnum.ORDER_TOTAL_PAGES,
					orderRepository.orderTotalPagesForCustomer(customerId, Arrays.asList()));
			orderList = orderRepository.findByCustomer(customerId, CommonUtil.getDefaultPage(offset));
		}
		return orderList;
	}

	private static PageRequest getPageRequest(Integer offset, Integer sortByCreationDate, Integer sortByPickupDate) {
		if (offset == null) {
			offset = 0;
		}
		PageRequest pageRequest = (PageRequest) CommonUtil.getDefaultPage(offset);
		if (sortByCreationDate != null) {
			Sort creationDate = Sort.by("creationDate").ascending();
			if (sortByCreationDate == -1) {
				creationDate = Sort.by("creationDate").descending();
			}
			pageRequest = PageRequest.of(offset, Constants.PAGINATION_SIZE, creationDate);
		} else if (sortByPickupDate != null) {
			Sort deliveryTime = Sort.by("deliveryTime").ascending();
			if (sortByPickupDate == -1) {
				deliveryTime = Sort.by("deliveryTime").descending();
			}
			pageRequest = PageRequest.of(offset, Constants.PAGINATION_SIZE, deliveryTime);
		}
		return pageRequest;
	}

	private List<Integer> getOrderStatus(Integer status) {
		List<Integer> orderStatus = new ArrayList<>();
		if (status == OrderStatus.FILTER_NEW_ORDERS.getValue()) {
			return PharmaUtil.NEW_ORDER_STATUS;
		} else if (status == OrderStatus.FILTER_PENDING_ORDERS.getValue()) {
			return PharmaUtil.PENDING_ORDER_STATUS;
		} else if (status == OrderStatus.FILTER_COMPLETED_ORDERS.getValue()) {
			return PharmaUtil.COMPLETED_ORDER_STATUS;
		}
		return orderStatus;
	}

	public List<Order> find(Integer offset, Integer status, Integer sortByCreationDate, Integer sortByPickupDate) {
		List<Order> orderList = null;
		if (userAuthService.isStoreOwner()) {
			Integer storeId = storeService.getCurrentUser().getId();
			setRequestParam(RequestParamEnum.ORDER_TOTAL_PAGES,
					orderRepository.orderTotalPagesForStore(storeId, getOrderStatus(status)));
			orderList = orderRepository.findByStore(storeId, getOrderStatus(status),
					getPageRequest(offset, sortByCreationDate, sortByPickupDate));
		} else {
			Integer customerId = customerService.getCurrentUser().getId();
			setRequestParam(RequestParamEnum.ORDER_TOTAL_PAGES,
					orderRepository.orderTotalPagesForCustomer(customerId, getOrderStatus(status)));
			orderList = orderRepository.findByCustomer(customerId,
					getPageRequest(offset, sortByCreationDate, sortByPickupDate));
		}
		return orderList;
	}

	public List findOrdersWithoutChildren(Integer offset, Integer status, Integer sortByCreationDate,
			Integer sortByPickupDate, boolean isStoreDataNeeded) {
		List<Order> returnOrder = find(offset, status, sortByCreationDate, sortByPickupDate);
		if (returnOrder != null) {
			List<OrderResponse> orderResponseList = new ArrayList<>();
			returnOrder.stream().forEach(order -> {
				orderResponseList.add(getOrderResponse(order, false));
			});
			return orderResponseList;
		}
		return null;
	}

	public OrderResponse getOrder(String orderNumber) {
		Integer storeId = storeService.getCurrentUserStoreId();
		Order order = orderRepository.findByOrderNumber(orderNumber, storeId);
		if (order == null) {
			ErrorHolder.throwError("given order with order number " + orderNumber + " does not exist");
		} else {
			return getOrderResponse(order, true);
		}
		return null;
	}

	public Integer getTotalPages() {
		return (Integer) getRequestParam(RequestParamEnum.ORDER_TOTAL_PAGES);
	}

	public Order updateOrder(OrderRequest orderRequest) {
		Integer storeId = storeService.getCurrentUserStoreId();
		Order order = orderRepository.findByOrderNumber(orderRequest.getOrderNumber(), storeId);
		if (order == null) {
			ErrorHolder
					.throwError("given order with order number " + orderRequest.getOrderNumber() + " does not exist");
		} else {
			setRequestParam(RequestParamEnum.EXISTING_ORDER, CopyUtil.deepCopy(order));
			order.setDeliveryTime(orderRequest.getDeliveryTime());
			order.setComments(orderRequest.getComments());
			order.setEstimatedPrice(orderRequest.getEstimatedPrice());
			order.setStatus(orderRequest.getStatus());
			order.setBillNumber(orderRequest.getBillNumber());
			if (OrderStatus.PHARMACY_EDITS_ORDER.getValue().equals(orderRequest.getStatus())) {
				List<OrderDetails> orderDetailsToBeSavedList = new ArrayList<>();
				for (OrderDetailsRequest orderDetailsRequest : orderRequest.getOrderDetails()) {
					Integer status = orderDetailsRequest.getStatus();
					if (OrderDetailStatus.DELETED.equals(status)) {
						orderDetailsService.delete(order, orderDetailsRequest.getId());
					} else if (OrderDetailStatus.ADDED.equals(status) || OrderDetailStatus.UPDATED.equals(status)) {
						String mediId = orderDetailsRequest.getMediId();
						String alternateMediId = orderDetailsRequest.getAlternateMediId();
						Integer quantity = orderDetailsRequest.getQuantity();
						if (quantity == null || quantity <= 0) {
							ErrorHolder.throwError("quantity cannot be null or less than or equal to 0");
						}
						OrderDetails orderDetail = null;
						if (OrderDetailStatus.UPDATED.equals(status)) {
							orderDetail = orderDetailsService.findById(orderDetailsRequest.getId());
							orderDetail.setQuantity(quantity);
							MedicalMaster alternateMedicine = null;
							if (alternateMediId != null) {
								alternateMedicine = medicineService.getMedicineById(alternateMediId);
								if (alternateMedicine == null) {
									ErrorHolder.throwError("medicine not found ::: " + alternateMediId);
								}
							}
							orderDetail.setAlternateMedicine(alternateMedicine);
						} else {
							MedicalMaster medicineById = medicineService.getMedicineById(mediId);
							if (medicineById == null) {
								ErrorHolder.throwError("medicine not found ::: " + mediId);
							}
							orderDetail = new OrderDetails(order, medicineById, quantity, null);
						}
						orderDetailsToBeSavedList.add(orderDetail);
					}
				}
				orderDetailsService.save(orderDetailsToBeSavedList);
			}
		}
		return save(order);
	}

	public Order updateOrderStatus(String orderNumber, Integer userProvidedStatus, String billNumber) {
		Integer storeId = storeService.getCurrentUserStoreId();
		Order order = orderRepository.findByOrderNumber(orderNumber, storeId);
		if (order == null) {
			ErrorHolder.throwError("Order number does not exist");
		} else {
			setRequestParam(RequestParamEnum.EXISTING_ORDER, CopyUtil.deepCopy(order));
			int currentStatus = order.getStatus();
			Boolean storeOwner = userAuthService.isStoreOwner();
			if (userProvidedStatus == OrderStatus.ORDER_ACCEPT.getValue()) {
				if (storeOwner) {
					userProvidedStatus = OrderStatus.PHARMACY_ACCEPTS_ORDER.getValue();
				} else {
					userProvidedStatus = OrderStatus.CUSTOMER_ACCEPTS_REVISION.getValue();
				}
			} else if (userProvidedStatus == OrderStatus.ORDER_REJECT.getValue()) {
				if (currentStatus == OrderStatus.ORDER_NEW.getValue()) {
					if (storeOwner) {
						userProvidedStatus = OrderStatus.PHARMACY_REJECTS_ORDER.getValue();
					} else {
						userProvidedStatus = OrderStatus.CUSTOMER_CANCELS_BEFORE_PHARMACY_ACCEPTS_OR_REJECTS.getValue();
					}
				} else if (!storeOwner) {
					// customer cases
					if (currentStatus == OrderStatus.PHARMACY_EDITS_ORDER.getValue()) {
						userProvidedStatus = OrderStatus.CUSTOMER_REJECTS_REVISION.getValue();
					} else if (currentStatus == OrderStatus.PHARMACY_ACCEPTS_ORDER.getValue()) {
						userProvidedStatus = OrderStatus.CUSTOMER_CANCELS_BEFORE_PICKUP.getValue();
					}
				}
			} else if (userProvidedStatus == OrderStatus.ORDER_COMPLETED.getValue()) {
				userProvidedStatus = OrderStatus.PHARMACY_DELIVERED.getValue();
				order.setBillNumber(billNumber);
			}
			order.setStatus(userProvidedStatus);
		}
		return save(order);
	}

	public List findOrderBySearchTerm(String searchTerm) {
		List<Order> returnOrder = new ArrayList<>();
		if (userAuthService.isStoreOwner()) {
			Integer storeId = storeService.findCurrentStore().getId();
			Order order = orderRepository.findByOrderNumberAndStore(storeId, searchTerm);
			if (order != null) {
				returnOrder.add(order);
			} else {
				returnOrder = orderRepository.getByMobileNumberAndStore(storeId, searchTerm);
			}
		}
		if (returnOrder != null) {
			List<OrderResponse> orderResponseList = new ArrayList<>();
			returnOrder.stream().forEach(order -> {
				orderResponseList.add(getOrderResponse(order, false));
			});
			return orderResponseList;
		}
		return null;
	}

	private OrderResponse getOrderResponse(Order order, boolean isPrescriptionNeeded) {
		OrderResponse orderResponse = new OrderResponse();

		// copying all fields
		orderResponse.setId(order.getId());
		orderResponse.setCreationDate(order.getCreationDate());
		orderResponse.setDeliveryTime(order.getDeliveryTime());
		orderResponse.setEstimatedPrice(order.getEstimatedPrice());
		orderResponse.setOrderNumber(order.getOrderNumber());
		orderResponse.setStatus(order.getStatus());
		orderResponse.setUpdatedDate(order.getUpdatedDate());
		orderResponse.setComments(order.getComments());
		orderResponse.setBillNumber(order.getBillNumber());
		Customer customer = order.getCustomer();
		UserAuth userAuth = userAuthService.findById(customer.getUserAuthId());
		customer.setMobileNumber(userAuth.getMobileNumber());
		orderResponse.setCustomer(customer);

		if (isPrescriptionNeeded) {
			orderResponse.setStore(order.getStore());
			List<OrderDetails> orderDetailsList = order.getOrderDetailsList();
			List<PrescriptionDetails> prescriptionDetailsList = new ArrayList<>();
			if (orderDetailsList != null) {
				orderDetailsList.stream().filter(orderDetails -> orderDetails.getPrescriptionId() != null)
						.map(orderDetails -> {
							PrescriptionDetails prescriptionDetails = new PrescriptionDetails();
							prescriptionDetails.setPrescriptionId(orderDetails.getId());
							Integer imageId = orderDetails.getPrescriptionId();
							Image image = imageService.findById(imageId);
							prescriptionDetails.setFileName(image.getFileName());
							prescriptionDetails.setPrescriptionImageUrl("/api/image/prescription?id=" + imageId);
							prescriptionDetailsList.add(prescriptionDetails);
							return orderDetails;
						}).collect(Collectors.toList());
				orderDetailsList = orderDetailsList.stream()
						.filter(orderDetails -> orderDetails.getPrescriptionId() == null).collect(Collectors.toList());
				orderResponse.setPrescriptionDetails(prescriptionDetailsList);
			}
			List<OrderDetailsResponse> orderDetailsResponseList = new ArrayList<>();
			if (orderDetailsList != null) {
				orderDetailsList.stream().forEach(orderDetails -> {
					orderDetailsResponseList.add(new OrderDetailsResponse(orderDetails, medicineImagePath));
				});
			}
			orderResponse.setOrderDetailsList(orderDetailsResponseList);
		}
		return orderResponse;
	}

	@Override
	protected void postSave(Order order, EntityState entityState) {
		orderAuditService.auditOrder(order, entityState.getValue());
		sendOrderStatusUpdateMessage(order);
		super.postSave(order, entityState);
	}

	private void sendOrderStatusUpdateMessage(Order updatedOrder) {
		Order existingOrder = (Order) getRequestParam(RequestParamEnum.EXISTING_ORDER);
		if (existingOrder == null) {
			String pharmaOrderMessage = null;
			String consumerOrderMessage = null;
			if (updatedOrder.isMedicineAvailable() && updatedOrder.isPrescAvailable())
				pharmaOrderMessage = PharmaResourceBundle.getProperty(
						ResourceBundleConstants.CONSUMER_ORDER_WITH_MEDICINE_AND_PRESCRIPTION,
						new Object[] { updatedOrder.getOrderNumber(), updatedOrder.getCustomer().getFirstName() });
			else
				pharmaOrderMessage = PharmaResourceBundle.getProperty(
						ResourceBundleConstants.CONSUMER_ORDER_WITH_ONLY_PRESCRIPTION,
						new Object[] { updatedOrder.getOrderNumber(), updatedOrder.getCustomer().getFirstName() });
			consumerOrderMessage = PharmaResourceBundle.getProperty(ResourceBundleConstants.CONSUMER_PLACES_NEW_ORDER,
					new Object[] { updatedOrder.getOrderNumber(), updatedOrder.getStore().getDisplayName() });
			storeService.sendMessage(updatedOrder.getStore(), pharmaOrderMessage);
			customerService.sendMessage(updatedOrder.getCustomer(), consumerOrderMessage);
			log.info("SMS sent to both customer {} and Store {} for the order no {}",
					updatedOrder.getCustomer().getFirstName(), updatedOrder.getStore().getDisplayName(),
					updatedOrder.getOrderNumber());

		} else if (!existingOrder.getStatus().equals(updatedOrder.getStatus())) {
			if (updatedOrder.getStatus() == OrderStatus.PHARMACY_ACCEPTS_ORDER.getValue()) {
				String orderMessage = PharmaResourceBundle.getProperty(ResourceBundleConstants.PHARMACY_ACCEPTS_ORDER,
						new Object[] { updatedOrder.getOrderNumber(), "Accepted",
								updatedOrder.getStore().getDisplayName() });

				customerService.sendMessage(updatedOrder.getCustomer(), orderMessage);
				log.info("SMS sent to the customer {} after accepting by {}", updatedOrder.getCustomer().getFirstName(),
						updatedOrder.getStore().getDisplayName());
			} else if (updatedOrder.getStatus() == OrderStatus.PHARMACY_EDITS_ORDER.getValue()) {
				String orderMessage = PharmaResourceBundle.getProperty(ResourceBundleConstants.PHARMACY_REVISED_ORDER,
						new Object[] { updatedOrder.getOrderNumber(), updatedOrder.getStore().getDisplayName() });

				customerService.sendMessage(updatedOrder.getCustomer(), orderMessage);
				log.info("SMS sent to the customer {} after revised by {}", updatedOrder.getCustomer().getFirstName(),
						updatedOrder.getStore().getDisplayName());

			} else if (updatedOrder.getStatus() == OrderStatus.PHARMACY_REJECTS_ORDER.getValue()) {

				String orderMessage = PharmaResourceBundle.getProperty(ResourceBundleConstants.PHARMACY_REJECTS_ORDER,
						new Object[] { updatedOrder.getOrderNumber(), "Rejected",
								updatedOrder.getStore().getDisplayName() });

				customerService.sendMessage(updatedOrder.getCustomer(), orderMessage);
				log.info("SMS sent to the customer {} after rejected by {}", updatedOrder.getCustomer().getFirstName(),
						updatedOrder.getStore().getDisplayName());
			}
		} else if (!existingOrder.getDeliveryTime().equals(updatedOrder.getDeliveryTime())) {
			String orderMessage = "Order " + updatedOrder.getOrderNumber() + "'s delivery time is changed";
			customerService.sendMessage(updatedOrder.getCustomer(), orderMessage);
		}
	}
}
