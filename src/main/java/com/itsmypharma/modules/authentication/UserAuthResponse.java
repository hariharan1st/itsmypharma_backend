package com.itsmypharma.modules.authentication;

import com.itsmypharma.modules.customer.Customer;
import com.itsmypharma.modules.store.Store;
import com.itsmypharma.common.util.CommonUtil;
import lombok.Data;

@Data
public class UserAuthResponse extends UserAuth {
    Customer customer;
    Store store;

    public UserAuthResponse(UserAuth userAuth) {
        this.setId(userAuth.getId());
        this.setStoreId(userAuth.getStoreId());
        this.setMobileNumber(userAuth.getMobileNumber());
        this.setCreationDate(userAuth.getCreationDate());
        this.setUpdatedDate(userAuth.getUpdatedDate());
        this.setUserCreated(userAuth.isUserCreated());
        CommonUtil.copyNonNullProperties(userAuth, this);
    }
}
