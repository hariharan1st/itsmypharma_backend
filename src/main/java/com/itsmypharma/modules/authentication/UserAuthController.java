package com.itsmypharma.modules.authentication;

import com.itsmypharma.common.generics.GenericRestController;
import com.itsmypharma.common.util.ResponseData;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/api/user")
@Slf4j
public class UserAuthController extends GenericRestController<UserAuth, UserAuthService> {

    @Autowired
    UserAuthService userAuthService;

    public UserAuthController() {
        super(null);
    }

    @Override
    protected UserAuthService getService() {
        return userAuthService;
    }

    @Transactional
    @RequestMapping(value = "/register_phone_number", produces = MediaType.APPLICATION_JSON_UTF8_VALUE, method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.OK)
    public ResponseData registerUser(@RequestBody Map requestBody) {
        String phoneNumber = (String) requestBody.get("phoneNumber");
        userAuthService.createUserByPhoneNumber(phoneNumber);
        Map response = new HashMap();
        response.put("otpValidity", 120);
        return ResponseData.setSuccess(response);
    }

    @Transactional
    @RequestMapping(value = "/verify_user", produces = MediaType.APPLICATION_JSON_UTF8_VALUE, method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.OK)
    public ResponseData verifyUser(@RequestBody Map requestBody) {
        String phoneNumber = (String) requestBody.get("phoneNumber");
        String otp = (String) requestBody.get("otp");
        UserAuthResponse userAuth = userAuthService.verifyUser(phoneNumber, otp);
        return ResponseData.setSuccess(userAuth);
    }
}
