package com.itsmypharma.modules.authentication;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.itsmypharma.common.exceptions.ErrorHolder;
import com.itsmypharma.common.generics.GenericService;
import com.itsmypharma.common.scope.RequestParamEnum;
import com.itsmypharma.common.util.CommonUtil;
import com.itsmypharma.constants.PharmaResourceBundle;
import com.itsmypharma.constants.ResourceBundleConstants;
import com.itsmypharma.modules.authentication.audit.login.LoginAuditService;
import com.itsmypharma.modules.customer.Customer;
import com.itsmypharma.modules.customer.CustomerService;
import com.itsmypharma.modules.services.MobileOtpService;
import com.itsmypharma.modules.store.Store;
import com.itsmypharma.modules.store.StoreService;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class UserAuthService extends GenericService<UserAuth> {
	@Autowired
	UserAuthRepository userAuthRepository;
	@Autowired
	StoreService storeService;
	@Autowired
	CustomerService customerService;
	@Autowired
	MobileOtpService mobileOtpService;
	@Autowired
	LoginAuditService loginAuditService;

	public UserAuthService(UserAuthRepository userAuthRepository) {
		super(userAuthRepository, "User Auth");
	}

	public UserAuth getCurrentUserAuth() {
		UserAuth userAuth = (UserAuth) getRequestParam(RequestParamEnum.CURRENT_USER_AUTH);
		if (userAuth == null) {
			Integer currentCustomerId = CommonUtil.getCurrentUserAuthId();
			if (currentCustomerId != null) {
				userAuth = super.findById(currentCustomerId);
				setRequestParam(RequestParamEnum.CURRENT_USER_AUTH, userAuth);
			}
		}
		return userAuth;
	}

	private Integer getStoreId() {
		Store storeFromRequest = storeService.findStoreFromRequest();
		Integer storeIdFromRequest = null;
		if (storeFromRequest != null) {
			storeIdFromRequest = storeFromRequest.getId();
		}
		return storeIdFromRequest;
	}

	public void createUserByPhoneNumber(String phoneNumber) {
		Integer storeIdFromRequest = getStoreId();
		UserAuth userAuth = findByMobileNoAndStoreId(phoneNumber, storeIdFromRequest);
		Store store = null;
		if (userAuth == null) {
			userAuth = new UserAuth();
			userAuth.setStoreId(storeIdFromRequest);
			if (storeIdFromRequest != null) {
				store = storeService.findById(storeIdFromRequest);
			}
		} else {
			if (userAuth.getStoreId() != null) {
				store = storeService.findById(userAuth.getStoreId());
				if (store == null) {
					ErrorHolder.throwError("given store does not exist");
				}
			}
		}
		userAuth.setMobileNumber(phoneNumber);
		String otp = CommonUtil.getOtp(4);
		userAuth.setOtp(otp);
		userAuth.setOtpDate(System.currentTimeMillis());
		save(userAuth);
		String message = PharmaResourceBundle.getProperty(ResourceBundleConstants.PHARMACY_SEND_OTP,
				new Object[] { otp });
		if (store != null) {
			message=null;
			message = PharmaResourceBundle.getProperty(ResourceBundleConstants.CONSUMER_SEND_OTP,
					new Object[] { store.getDisplayName(), otp });
		}
		mobileOtpService.sendMessage(message, "91" + phoneNumber);
		log.info("Login Otp sent to the mobile number {}, {}",phoneNumber, new Date());
	}

	private UserAuth findByMobileNoAndStoreId(String phoneNumber, Integer storeIdFromRequest) {
		return userAuthRepository.findByMobileNumberAndStoreId(phoneNumber, storeIdFromRequest);
	}

	public UserAuthResponse verifyUser(String phoneNumber, String otp) {
		Integer storeId = getStoreId();
		UserAuth userAuth = findByMobileNoAndStoreId(phoneNumber, storeId);
		if (userAuth == null) {
			ErrorHolder.throwError("user not found for given phone number" + phoneNumber);
		}
		long otpTime = userAuth.getOtpDate();
		long currentTime = System.currentTimeMillis();
		if (currentTime - otpTime > 120000) {
			ErrorHolder.throwError("OTP expired");
		}
		if (!userAuth.getOtp().equals(otp)) {
			ErrorHolder.throwError("Invalid OTP");
		}
		userAuth.setOtp("");
		userAuth.setOtpDate(0L);
		userAuth.setUserCreated(customerService.findByUserAuthId(userAuth.getId()) != null
				|| storeService.findByUserAuthId(userAuth.getId()) != null);
		save(userAuth);
		// audit only store logins as of now
		if (storeId == null) {
			loginAuditService.onSuccessfulLogin(userAuth.getMobileNumber());
		}
		return getUserAuthResponse(userAuth);
	}

	private UserAuthResponse getUserAuthResponse(UserAuth userAuth) {
		UserAuthResponse userAuthResponse = new UserAuthResponse(userAuth);
		Customer customer = customerService.findByUserAuthId(userAuth.getId());
		if (customer == null && getStoreIdFromRequestPassivated() != null) {
			customer = new Customer();
			customer.setUserAuthId(userAuth.getId());
		}
		userAuthResponse.setCustomer(customer);
		Store store = storeService.findByUserAuthId(userAuth.getId());
		if (store != null) {
			userAuthResponse.setStoreId(store.getId());
			userAuthResponse.setCustomer(null);
		} else if (getStoreIdFromRequestPassivated() == null) {
			store = new Store();
			store.setUserAuthId(userAuth.getId());
			userAuthResponse.setCustomer(null);
		}
		userAuthResponse.setStore(store);
		return userAuthResponse;
	}

	public Boolean isStoreOwner() {
		UserAuth currentUserAuth = getCurrentUserAuth();
		if (currentUserAuth == null) {
			return false;
		}
		return currentUserAuth.getStoreId() == null;
	}

}
