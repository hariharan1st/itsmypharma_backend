package com.itsmypharma.modules.authentication.audit.login;

public class LoginAuditStatus {
    public static Integer LOGIN_SUCESS = 0;
    public static Integer LOGIN_FAILED = 1;
}
