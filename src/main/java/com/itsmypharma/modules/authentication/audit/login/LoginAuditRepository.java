package com.itsmypharma.modules.authentication.audit.login;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LoginAuditRepository extends CrudRepository<LoginAudit, Integer> {
}
