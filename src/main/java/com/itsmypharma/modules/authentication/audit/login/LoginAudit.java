package com.itsmypharma.modules.authentication.audit.login;

import com.itsmypharma.common.generics.GenericEntity;
import lombok.Data;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "pharma_login_audit")
@DynamicInsert
@DynamicUpdate
@Data
public class LoginAudit extends GenericEntity<Integer> {

    @GeneratedValue(strategy = GenerationType.AUTO)
    @Id
    @Column(name = "la_id")
    private Integer id;

    @Column(name = "la_mobile_no")
    private String mobileNumber;
    @Column(name = "la_status")
    private Integer status;
    @Column(name = "la_creation_date")
    private Long creationDate;
    @Transient
    private Long updatedDate;
}
