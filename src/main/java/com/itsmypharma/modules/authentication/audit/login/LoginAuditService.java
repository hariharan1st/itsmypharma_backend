package com.itsmypharma.modules.authentication.audit.login;

import com.itsmypharma.common.generics.GenericService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LoginAuditService extends GenericService<LoginAudit> {
    @Autowired
    LoginAuditRepository loginAuditRepository;

    public LoginAuditService(LoginAuditRepository loginAuditRepository) {
        super(loginAuditRepository, "Login Audit");
    }

    public void onSuccessfulLogin(String mobileNumber) {
        LoginAudit loginAudit = new LoginAudit();
        loginAudit.setMobileNumber(mobileNumber);
        loginAudit.setStatus(LoginAuditStatus.LOGIN_SUCESS);
        save(loginAudit);
    }
}
