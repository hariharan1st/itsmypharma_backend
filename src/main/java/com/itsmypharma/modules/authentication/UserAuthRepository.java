package com.itsmypharma.modules.authentication;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserAuthRepository extends CrudRepository<UserAuth, Integer> {
    UserAuth findByMobileNumberAndStoreId(String phoneNumber, Integer storeIdFromRequest);
}
