package com.itsmypharma.modules.authentication;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.itsmypharma.common.generics.GenericEntity;
import lombok.Data;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.io.Serializable;


@Entity
@Table(name = "pharma_user_auth")
@DynamicInsert
@DynamicUpdate
@Cacheable(value = false)
@Data
public class UserAuth extends GenericEntity<Integer> implements Serializable {
    private static final long serialVersionUID = 1L;

    @GeneratedValue(strategy = GenerationType.AUTO)
    @Id
    @Column(name = "pua_auth_id")
    private Integer id;

    @JsonIgnore
    @Column(name = "pua_otp")
    private String otp;

    @Column(name = "pua_store_id")
    private Integer storeId;

    @Column(name = "pua_mobile_no")
    private String mobileNumber;

    @Column(name = "pua_creation_date")
    private Long creationDate;

    @Column(name = "pua_updated_date")
    private Long updatedDate;

    @JsonIgnore
    @Column(name = "pua_otp_time")
    private Long otpDate;

    @Transient
    private boolean isUserCreated;

}
